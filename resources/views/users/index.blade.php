@extends('layout.app')

@section('content')
<nav aria-label="Page breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Users</li>
    </ol>
</nav>
<a href="{{route('users.create')}}" class="btn btn-success btn-sm"><small>Add new</small></a>
<p></p>
<div class="app-card shadow-sm mb-4 border-left-decoration">
    <div class="inner">
        <div class="app-card-body p-4">
            <div class="row gx-5 gy-3">
                <div class="col-12 col-md-12 col-sm-12">

                    <table class="table table-light" id="users_table">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Role</th>
                                <th>Date Added</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($users as $user)
                            <tr>
                                <td>{{$user->id}}</td>
                                <td class="text-uppercase">{{$user->first_name}} {{$user->middle_name ?? $user->middle_name}} {{$user->last_name}}</td>
                                <td class="text-uppercase">{{$user->getRoleNames()[0]}}</td>
                                <td class="text-uppercase">{{$user->created_at}}</td>
                                <td>
                                    <form action="{{route('users.destroy', $user)}}" method="post">
                                        <a href="{{route('users.edit', $user)}}" class="btn btn-info" type="button">Edit</a>
                                        @csrf
                                        @method('DELETE')

                                        <button class="btn btn-danger" type="submit">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="6">No record found.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <!--//col-->
            </div>
            <!--//row-->

        </div>
        <!--//app-card-body-->

    </div>
    <!--//inner-->
</div>

@endsection

@section('js')
<script>
    $(function() {
        $('#users_table').DataTable();

    })
</script>
@endsection