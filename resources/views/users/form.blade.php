@extends('layout.app')

@section('content')
<nav aria-label="Page breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item" aria-current="page"><a href="{{route('users.index')}}">Users</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{isset($user->id) ? 'Update' : 'Add new' }} </li>
    </ol>
</nav>
<div class="app-card shadow-sm mb-4 border-left-decoration">
    <div class="inner">
        <div class="app-card-body p-4">
            <form action="{{isset($user->id) ? route('users.update', $user->id) :route('users.store')}}" method="post" enctype="multipart/form-data">
                <div class="row gx-5 gy-3">
                    <div class="col-md-6 ">
                        @csrf
                        @if(isset($user->id))
                        @method('PUT')
                        @endif
                        <div class="row">
                            <div class="form-group">
                                <label for="first_name">First Name:</label>
                                <input id="first_name" class="form-control" value="{{old('first_name', $user->first_name)}}" type="text" name="first_name" placeholder="Juan" required>
                            </div>
                            <div class="form-group">
                                <label for="middle_name">Middle Name:</label>
                                <input id="middle_name" class="form-control" value="{{old('middle_name', $user->middle_name)}}" type="text" name="middle_name" placeholder="P" required>
                            </div>
                            <div class="form-group">
                                <label for="last_name">Last Name:</label>
                                <input id="last_name" class="form-control" value="{{old('last_name', $user->last_name)}}" type="text" name="last_name" placeholder="Dela Cruz" required>
                            </div>
                            <div class="form-group">
                                <label for="prc_no">PRC Number:</label>
                                <input id="prc_no" class="form-control" value="{{old('prc_no', $user->prc_no)}}" type="text" name="prc_no" placeholder="001234" required>
                            </div>
                            <div class="form-group">
                                <label for="position">Position:</label>
                                <input id="position" class="form-control" value="{{old('position', $user->position)}}" type="text" name="position" placeholder="Medical Technologist" required>
                            </div>
                            <div class="form-group">
                                <label for="middle_name">Email Address:</label>
                                <input id="middle_name" class="form-control" value="{{old('email', $user->email)}}" type="email" name="email" placeholder="jdelacruz@gmail.com">
                            </div>
                            <div class="form-group">
                                <label for="last_name">Password:</label>
                                <input id="last_name" class="form-control" type="password" name="password" required>
                            </div>

                            <div class="form-group">
                                <label for="my-select">Role</label>
                                <select id="my-select" class="form-control" name="role">
                                    <option value="P">Pathologist</option>
                                    <option value="MT">Med Tech</option>
                                </select>
                            </div>

                        </div>
                        <p></p>
                        <button class="btn btn-success" type="submit">{{isset($user->id) ? 'Update' : 'Save'  }}</button>
                        <a href="{{route('users.index')}}" class="btn btn-danger" type="button">Cancel</a>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">

                            <label for="formFile" class="form-label">E-Signature</label>
                            <input class="form-control" type="file" id="formFile" name="esign" {{isset($user->id) ? '' : 'required'}}>
                        </div>
                        @if(isset($user->id))
                        <div class="form-group">
                            <p>Current E-Sign:</p>
                            <img class="img-fluid w-50" src="{{asset('storage/'.$user->signature)}}" alt="">
                        </div>
                        @endif
                    </div>
                    <!--//col-->
                </div>
                <!--//row-->
            </form>

        </div>
        <!--//app-card-body-->

    </div>
    <!--//inner-->
</div>

@endsection