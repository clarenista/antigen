@extends('layout.app')

@section('content')
<nav aria-label="Page breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Brands</li>
    </ol>
</nav>
<div class="row">
    <div class="col-md-4 col-sm-12">
        <div class="app-card shadow-sm mb-4 border-left-decoration">
            <div class="inner">
                <div class="app-card-body p-4">
                    <div class="row gx-5 gy-3">
                        <div class="col-md col-sm-12">
                            <form method="POST" action="{{isset($brand->id) ? route('brands.update', $brand) : route('brands.store')}}">
                                @csrf
                                @if(isset($brand->id))
                                @method('PUT')
                                @endif
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input id="name" class="form-control" value="{{old('name', $brand->name)}}" type="text" name="name">
                                </div>
                                <div class="form-group">
                                    <label for="manufacturer">Manufacturer</label>
                                    <input id="manufacturer" class="form-control" value="{{old('manufacturer', $brand->manufacturer)}}" type="text" name="manufacturer">
                                </div>
                                <p></p>
                                <button class="btn btn-primary" type="submit">Save</button>
                                @if(isset($brand->id))
                                <a href="{{route('brands.index')}}" class="btn btn-warning" type="button">Cancel</a>
                                @endif
                            </form>
                        </div>
                        <!--//col-->
                    </div>
                    <!--//row-->

                </div>
                <!--//app-card-body-->

            </div>
            <!--//inner-->
        </div>

    </div>
    <div class="col-md col-sm-12">
        <table class="table table-light table-hover" id="brands_table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($brands as $brand)
                <tr>
                    <td>{{$brand->id}}</td>
                    <td>{{$brand->name}}</td>
                    <td>{{$brand->manufacturer}}</td>
                    <td>
                        <form action="{{route('brands.destroy', $brand)}}" method="POST">
                            <a href="{{route('brands.edit', $brand)}}" class="btn btn-primary" type="button">Edit</a>
                            @csrf
                            @method("DELETE")
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="4">No record found.</td>
                </tr>
                @endforelse
            </tbody>
        </table>

    </div>
</div>
@endsection

@section('js')
<script>
    $(function() {
        $('#brands_table').DataTable()
    })
</script>
@endsection