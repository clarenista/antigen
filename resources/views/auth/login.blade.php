@extends('layout.public')

@section('content')

<?php
$facility_details = \App\Models\Setting::where('item', 'facility_details')->orderBy('created_at', 'desc')->pluck('value')->first();
$parsed = json_decode($facility_details, TRUE)
?>

<div class="row g-0 app-auth-wrapper">
    <div class="col-12 col-md-7 col-lg-6 auth-main-col text-center p-5">
        <div class="d-flex flex-column align-content-end">
            <div class="app-auth-body mx-auto">
                <div class="app-auth-branding mb-4"><a class="app-logo" href="index.html"><img class="logo-icon mr-2" src="{{asset('storage/' . $parsed['logo'])}}" alt="logo"></a></div>
                <h2 class="auth-heading text-center mb-5">Log in to Portal</h2>
                <div class="auth-form-container text-left">
                    <form class="auth-form login-form" form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="email mb-3">
                            <label class="sr-only" for="signin-email">Email</label>
                            <input id="signin-email" name="email" type="email" class="form-control signin-email @error('email') is-invalid @enderror" placeholder="Email address" required="required" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <!--//form-group-->
                        <div class="password mb-3">
                            <label class="sr-only" for="signin-password">Password</label>
                            <input id="signin-password" name="password" type="password" class="form-control signin-password @error('password') is-invalid @enderror" placeholder="Password" required autocomplete="current-password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <div class="extra mt-3 row justify-content-between">
                                <div class="col-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label class="form-check-label" for="remember">
                                            Remember me
                                        </label>
                                    </div>
                                </div>
                                <!--//col-6-->

                                <!--//col-6-->
                            </div>
                            <!--//extra-->
                        </div>
                        <!--//form-group-->
                        <div class="text-center">
                            <button type="submit" class="btn app-btn-primary btn-block theme-btn mx-auto">Log In</button>
                        </div>
                    </form>
                </div>
                <!--//auth-form-container-->

            </div>
            <!--//auth-body-->

        </div>
        <!--//flex-column-->
    </div>
    <!--//auth-main-col-->
    <div class="col-12 col-md-5 col-lg-6 h-100 auth-background-col">
        <div class="auth-background-holder">
        </div>
        <div class="auth-background-masks"></div>
        <div class="auth-background-overlay p-3 p-lg-5">
            <div class="d-flex flex-column align-content-end h-100">
                <div class="h-100"></div>
                <div class="overlay-content p-3 p-lg-4 rounded">
                    <h5 class="mb-3 overlay-title">{{strtoupper($parsed['name'])}}</h5>
                    <div>{{$parsed['address']}}</div>
                </div>
            </div>
        </div>
        <!--//auth-background-overlay-->
    </div>
    <!--//auth-background-col-->

</div>
<!--//row-->

@endsection