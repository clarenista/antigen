@extends('layout.app')

@section('content')
<nav aria-label="Page breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Physicians</li>
    </ol>
</nav>
<a href="{{route('physicians.create')}}" class="btn btn-success btn-sm"><small>Add new</small></a>
<p></p>
<div class="app-card shadow-sm mb-4 border-left-decoration">
    <div class="inner">
        <div class="app-card-body p-4">
            <div class="row gx-5 gy-3">
                <div class="col-12 col-md-12 col-sm-12">

                    <table class="table table-light" id="physicians_table">
                        <thead class="thead-light">
                            <tr>
                                <th>PRC No.</th>
                                <th>First name</th>
                                <th>Middle initial</th>
                                <th>Last Name</th>
                                <th>Suffix</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($physicians as $physician)
                            <tr>
                                <td>{{$physician->prc_no}}</td>
                                <td class="text-uppercase">{{$physician->first_name}}</td>
                                <td class="text-uppercase">{{$physician->middle_name}}</td>
                                <td class="text-uppercase">{{$physician->last_name}}</td>
                                <td class="text-uppercase">{{$physician->suffix}}</td>
                                <td>
                                    <form action="{{route('physicians.destroy', $physician)}}" method="post">
                                        <a href="{{route('physicians.edit', $physician)}}" class="btn btn-info" type="button">Edit</a>
                                        @csrf
                                        @method('DELETE')

                                        <button class="btn btn-danger" type="submit">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="6">No record found.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <!--//col-->
            </div>
            <!--//row-->

        </div>
        <!--//app-card-body-->

    </div>
    <!--//inner-->
</div>

@endsection

@section('js')
<script>
    $(function() {
        $('#physicians_table').DataTable();

    })
</script>
@endsection