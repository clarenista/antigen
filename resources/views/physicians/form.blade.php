@extends('layout.app')

@section('content')
<nav aria-label="Page breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item" aria-current="page"><a href="{{route('physicians.index')}}">Physicians</a></li>
        <li class="breadcrumb-item active" aria-current="page">Add new</li>
    </ol>
</nav>
<div class="app-card shadow-sm mb-4 border-left-decoration">
    <div class="inner">
        <div class="app-card-body p-4">
            <div class="row gx-5 gy-3">
                <div class="col-md-6 ">
                    <form action="{{isset($physician->id) ? route('physicians.update', $physician->id) :route('physicians.store')}}" method="post">
                        @csrf
                        @if(isset($physician->id))
                        @method('PUT')
                        @endif
                        <div class="row">
                            <div class="form-group">
                                <label for="first_name">First Name:</label>
                                <input id="first_name" class="form-control" value="{{old('first_name', $physician->first_name)}}" type="text" name="first_name" placeholder="Juan" required>
                            </div>
                            <div class="form-group">
                                <label for="middle_name">Middle Name:</label>
                                <input id="middle_name" class="form-control" value="{{old('middle_name', $physician->middle_name)}}" type="text" name="middle_name" placeholder="P.">
                            </div>
                            <div class="form-group">
                                <label for="last_name">Last Name:</label>
                                <input id="last_name" class="form-control" value="{{old('last_name', $physician->last_name)}}" type="text" name="last_name" placeholder="Dela Cruz" required>
                            </div>


                            <div class="form-group">
                                <label for="suffix">Suffix:</label>
                                <input id="suffix" class="form-control" value="{{old('suffix', $physician->suffix)}}" type="text" name="suffix" placeholder="MD" required>
                            </div>

                            <div class="form-group">
                                <label for="prc_no">PRC Number:</label>
                                <input id="prc_no" class="form-control" value="{{old('prc_no', $physician->prc_no)}}" type="text" name="prc_no" placeholder="12345" required>
                            </div>
                        </div>
                        <p></p>
                        <button class="btn btn-success" type="submit">Save</button>
                        <a href="{{route('physicians.index')}}" class="btn btn-danger" type="button">Cancel</a>
                    </form>
                </div>
                <div class="col-md-6">
                    <i class="fas fa-user-md fa-lg"></i>
                </div>
                <!--//col-->
            </div>
            <!--//row-->

        </div>
        <!--//app-card-body-->

    </div>
    <!--//inner-->
</div>

@endsection