@extends('layout.app')

@section('content')


<div class="app-card shadow-sm mb-4 border-left-decoration">
    <div class="inner">
        <div class="app-card-body p-4">
            <div class="row gx-5 gy-3">
                <div class="col-12 col-md-12 col-sm-12">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <button class="nav-link active" id="nav-logo-tab" data-bs-toggle="tab" data-bs-target="#nav-logo" type="button" role="tab" aria-controls="nav-logo" aria-selected="true">Facility Details</button>
                            <button class="nav-link" id="nav-auto-approve-tab" data-bs-toggle="tab" data-bs-target="#nav-auto-approve" type="button" role="tab" aria-controls="nav-auto-approve" aria-selected="true">Auto Approve</button>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-logo" role="tabpanel" aria-labelledby="nav-logo-tab">
                            <div class="row my-3">
                                <div class="col-md col-sm-12">
                                    <form action="{{route('settings.updateFacility')}}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="mb-3 form-group">
                                            <input type="hidden" name="item" value="facility_details">

                                            <label for="formFile" class="form-label">Logo</label>
                                            <input class="form-control" type="file" id="formFile" name="facility_logo">
                                        </div>
                                        <div class="form-group mb-2">
                                            <label for="name">Name</label>
                                            <input id="name" class="form-control" value="{{$parsed['name']}}" type="text" name="name" required>
                                        </div>
                                        <div class="form-group mb-2">
                                            <label for="contact">Contact No.</label>
                                            <input id="contact" class="form-control" value="{{$parsed['contact']}}" type="text" name="contact" required>
                                        </div>
                                        <div class="form-group mb-2">
                                            <label for="type">Type</label>
                                            <input id="type" class="form-control" value="{{$parsed['type']}}" type="text" name="type" required>
                                        </div>
                                        <div class="form-group mb-2">
                                            <label for="address">Address</label>
                                            <textarea id="address" class="form-control" name="address" rows="3" style="height: 100%" required>{{$parsed['address']}}</textarea>

                                        </div>

                                        <button class="btn btn-primary" type="submit">Update</button>
                                    </form>
                                </div>
                                <div class="col-md col-sm-12">
                                    <p>Current Logo</p>
                                    <img class="img-fluid w-50" src="{{isset($parsed['logo']) ? asset('storage/'.$parsed['logo']) : ''}}" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade show" id="nav-auto-approve" role="tabpanel" aria-labelledby="nav-auto-approve-tab">
                            <div class="row my-3">
                                <div class="col-md col-sm-12">
                                    <form action="{{route('settings.updateAutoApprove')}}" method="POST">
                                        @csrf
                                        <div class="form-group mb-3">
                                            <input type="hidden" name="item" value="auto_approve">
                                            <input type="hidden" name="id" value="{{$auto_approve->id}}">
                                            <div class="form-check form-switch">
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckAutoApprove" name="value" {{$auto_approve->value == 1 ? 'checked' : null}}>
                                                <label class="form-check-label" for="flexSwitchCheckAutoApprove">Auto Approve</label>
                                            </div>
                                            <hr>
                                        </div>
                                        <button class="btn btn-primary" type="submit">Update</button>
                                    </form>
                                </div>
                                <div class="col-md col-sm-12">
                                    <p class="lead">Auto Approve History:</p>
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--//col-->
            </div>
            <!--//row-->

        </div>
        <!--//app-card-body-->

    </div>
    <!--//inner-->
</div>

@endsection