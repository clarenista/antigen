@extends('layout.app')

@section('content')
<nav aria-label="Page breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Specimens</li>
    </ol>
</nav>
<div class="row">
    <div class="col-md-4 col-sm-12">
        <div class="app-card shadow-sm mb-4 border-left-decoration">
            <div class="inner">
                <div class="app-card-body p-4">
                    <div class="row gx-5 gy-3">
                        <div class="col-md col-sm-12">
                            <form method="POST" action="{{isset($specimen->id) ? route('specimens.update', $specimen) : route('specimens.store')}}">
                                @csrf
                                @if(isset($specimen->id))
                                @method('PUT')
                                @endif
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <input id="description" class="form-control" value="{{old('name', $specimen->description)}}" type="text" name="description" placeholder="Oropharyngeal swab">
                                </div>
                                <p></p>
                                <button class="btn btn-primary" type="submit">Save</button>
                                @if(isset($specimen->id))
                                <a href="{{route('specimens.index')}}" class="btn btn-warning" type="button">Cancel</a>
                                @endif
                            </form>
                        </div>
                        <!--//col-->
                    </div>
                    <!--//row-->

                </div>
                <!--//app-card-body-->

            </div>
            <!--//inner-->
        </div>

    </div>
    <div class="col-md col-sm-12">
        <table class="table table-light table-hover" id="specimens_table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Description</th>
                    <th>Date added</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($specimens as $specimen)
                <tr>
                    <td>{{$specimen->id}}</td>
                    <td>{{$specimen->description}}</td>
                    <td>{{$specimen->created_at}}</td>
                    <td>
                        <form action="{{route('specimens.destroy', $specimen)}}" method="POST">
                            <a href="{{route('specimens.edit', $specimen)}}" class="btn btn-primary" type="button">Edit</a>
                            @csrf
                            @method("DELETE")
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="4">No record found.</td>
                </tr>
                @endforelse
            </tbody>
        </table>

    </div>
</div>
@endsection

@section('js')
<script>
    $(function() {
        $('#specimens_table').DataTable()
    })
</script>
@endsection