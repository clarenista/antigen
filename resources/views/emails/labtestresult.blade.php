<div style="text-align: center;">
    <img src="{{asset('/images/logo.png')}}" width="100" />
    <!-- <img src="{{\url()->to('/')}}" /> -->
    <h2>Dear {{$patientName}},</h2>
    <h2>Your COVID-19 Rapid Antigen result is now available</h2>
    <p>Please click the link below for your COVID-19 Rapid Antigen Result . </p>
    <p>Should you have queries about this service,</p>
    <p>you may reach us at the following contact information:</p>
    <p><a href="{{\route('pdf', ['key' => $labTestResult->key, 'pub' => 'D' ])}}">{{\route('pdf', ['key' => $labTestResult->key, 'pub' => 'D' ])}}</a></p>
    <div style="padding: 100px; text-align: center; background-color: #eee; ">
        <p>test</p>
    </div>
</div>