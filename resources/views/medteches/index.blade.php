@extends('layout.app')

@section('content')
<nav aria-label="Page breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Med Technologist</li>
    </ol>
</nav>
<a href="{{route('medteches.create')}}" class="btn btn-success btn-sm"><small>Add new</small></a>
<p></p>
<div class="app-card shadow-sm mb-4 border-left-decoration">
    <div class="inner">
        <div class="app-card-body p-4">
            <div class="row gx-5 gy-3">
                <div class="col-12 col-md-12 col-sm-12">

                    <table class="table table-light" id="physicians_table">
                        <thead class="thead-light">
                            <tr>
                                <th>License No.</th>
                                <th>First name</th>
                                <th>Middle initial</th>
                                <th>Last Name</th>
                                <th>Suffix</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($medteches as $medtech)
                            <tr>
                                <td>{{$medtech->license_no}}</td>
                                <td class="text-uppercase">{{$medtech->first_name}}</td>
                                <td class="text-uppercase">{{$medtech->middle_name}}</td>
                                <td class="text-uppercase">{{$medtech->last_name}}</td>
                                <td class="text-uppercase">{{$medtech->suffix}}</td>
                                <td>
                                    <form action="{{route('medteches.destroy', $medtech)}}" method="post">
                                        <a href="{{route('medteches.edit', $medtech)}}" class="btn btn-info" type="button">Edit</a>
                                        @csrf
                                        @method('DELETE')

                                        <button class="btn btn-danger" type="submit">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="6">No record found.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <!--//col-->
            </div>
            <!--//row-->

        </div>
        <!--//app-card-body-->

    </div>
    <!--//inner-->
</div>

@endsection

@section('js')
<script>
    $(function() {
        $('#physicians_table').DataTable();

    })
</script>
@endsection