@extends('layout.app')

@section('content')
<nav aria-label="Page breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item" aria-current="page"><a href="{{route('patients.index')}}">Patients</a></li>
        <li class="breadcrumb-item " aria-current="page"><a href="{{route('patients.edit', $patient->id)}}">{{$patient->first_name}} {{$patient->last_name}} </a></li>
        <li class="breadcrumb-item " aria-current="page"><a href="{{route('patients.index_vax', $patient->id)}}">Vaccinations </a></li>
        <li class="breadcrumb-item active" aria-current="page">New Vaccination</li>
    </ol>
</nav>
<div class="row">
    <div class="col-md-6 col-sm-12">
        <div class="card">
            <div class="card-body">
                <form id="newVaxForm" method="POST" action="{{isset($vaccine->id) ? route('patients.update_vax', [$patient->id, $vaccine->id]): route('patients.store_vax', $patient->id)}}">
                    @csrf
                    @if(isset($vaccine->id))
                    @method('PUT')
                    @endif
                    <div class="form-group">
                        <label for="select_dose_no">Dose</label>
                        <select id="select_dose_no" class="form-control custom-select" name="dose_no" value="" required>
                            <option value="">--Select Dose--</option>
                            <option value="1" {{old('dose_no', $vaccine->dose_no) == 1 ? 'selected' : ''}}>First Dose</option>
                            <option value="2" {{old('dose_no', $vaccine->dose_no) == 2 ? 'selected' : ''}}>Second Dose</option>
                            <option value="3" {{old('dose_no', $vaccine->dose_no) == 3 ? 'selected' : ''}}>Booster</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="vaccination_date">Vaccination date:</label>
                        <input id="vaccination_date" class="form-control" type="date" value="{{old('vaccination_date', $vaccine->vaccination_date)}}" name="vaccination_date" required>
                    </div>
                    <div class="form-group">
                        <label for="vaccine_name">Vaccine name:</label>
                        <input id="vaccine_name" class="form-control" type="text" value="{{old('vaccine_name', $vaccine->vaccine_name)}}" name="vaccine_name" required>
                    </div>
                    <div class="form-group">
                        <label for="vaccination_facility">Vaccination Facility</label>
                        <input id="vaccination_facility" class="form-control" type="text" name="vaccination_facility" value="{{old('vaccination_facility', $vaccine->vaccination_facility)}}" name="vaccination_facility" required>
                    </div>
                    <div class="form-group">
                        <label for="facility_region">Facility Region</label>
                        <input id="facility_region" class="form-control" type="text" value="{{old('facility_region', $vaccine->facility_region)}}" name="facility_region" required>
                    </div>
                    <div class="form-group mb-3">
                        <div class="form-check">
                            <input id="is_adverse_event" class="form-check-input" type="checkbox" {{old('is_adverse_event', $vaccine->is_adverse_event) == 1 ? 'checked' : ''}} name="is_adverse_event">
                            <label for="is_adverse_event" class="form-check-label">Adverse Event/s?</label>
                        </div>
                    </div>
                    <button class="btn btn-secondary" type="submit">Save</button>
                </form>
            </div>

        </div>

    </div>
</div>



@endsection