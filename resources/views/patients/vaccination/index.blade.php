@extends('layout.app')

@section('content')
<nav aria-label="Page breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item" aria-current="page"><a href="{{route('patients.index')}}">Patients</a></li>
        <li class="breadcrumb-item " aria-current="page"><a href="{{route('patients.edit', $patient->id)}}">{{$patient->first_name}} {{$patient->last_name}} </a></li>
        <li class="breadcrumb-item active" aria-current="page">Vaccinations</li>
    </ol>
</nav>
<div class="card">

    <div class="card-body">
        <h3>Vaccination Information</h3>
        <a href="{{route('patients.create_vax', $patient->id)}}" class="btn btn-success btn-sm mb-3">Add vaccination</a>
        <table class="table table-responsive table-hover" id="vax_table">
            <thead class="thead-light">
                <tr>
                    <th>Dose</th>
                    <th>Name</th>
                    <th>Facility</th>
                    <th>Vaccination_date</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($patient->vaccinations as $vaccine)
                <tr>
                    @if($vaccine->dose_no == 1)
                    <td>First</td>
                    @elseif($vaccine->dose_no == 2)
                    <td>Second</td>
                    @else
                    <td>Booster</td>
                    @endif
                    <td>{{$vaccine->vaccine_name}}</td>
                    <td>{{$vaccine->vaccination_facility}}</td>
                    <td>{{$vaccine->vaccination_date}}</td>
                    <td>
                        <a href="{{route('patients.edit_vax', [$patient->id, $vaccine->id])}}" class="btn btn-info" type="button">Edit</a>
                        <button class="btn btn-danger" type="button">Delete</button>
                    </td>
                </tr>
                @empty
                @endforelse

            </tbody>

        </table>
    </div>

</div>
@endsection

@section('js')
<script>
    $('#vax_table').DataTable()
</script>
@endsection