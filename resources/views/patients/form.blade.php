@extends('layout.app')

@section('content')
<nav aria-label="Page breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item" aria-current="page"><a href="{{route('patients.index')}}">Patients</a></li>
        <li class="breadcrumb-item active" aria-current="page">Add new</li>
    </ol>
</nav>
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <h5>New Vaccination Details</h5>
                <hr>
                <form id="newVaxForm">
                    <div class="form-group">
                        <label for="select_dose_no">Dose</label>
                        <select id="select_dose_no" class="form-control custom-select" name="dose_no">
                            <option value="">--Select Dose--</option>
                            <option value="1">First Dose</option>
                            <option value="2">Second Dose</option>
                            <option value="3">Booster</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="vaccination_date">Vaccination date:</label>
                        <input id="vaccination_date" class="form-control" type="date" name="vaccination_date">
                    </div>
                    <div class="form-group">
                        <label for="vaccine_name">Vaccine name:</label>
                        <input id="vaccine_name" class="form-control" type="text" name="vaccine_name">
                    </div>
                    <div class="form-group">
                        <label for="vaccination_facility">Vaccination Facility</label>
                        <input id="vaccination_facility" class="form-control" type="text" name="vaccination_facility">
                    </div>
                    <div class="form-group">
                        <label for="facility_region">Facility Region</label>
                        <input id="facility_region" class="form-control" type="text" name="facility_region">
                    </div>
                    <div class="form-group mb-3">
                        <div class="form-check">
                            <input id="is_adverse_event" class="form-check-input" type="checkbox" name="is_adverse_event">
                            <label for="is_adverse_event" class="form-check-label">Adverse Event/s?</label>
                        </div>
                    </div>
                    <button class="btn btn-secondary" type="submit">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="app-card shadow-sm mb-4 border-left-decoration">
    <div class="inner">
        <div class="app-card-body p-4">
            <form action="{{isset($patient->id) ? route('patients.update', $patient->id) :route('patients.store')}}" method="post">
                <div class="row gx-5 gy-3">
                    <h3>Patient Information</h3>
                    <div class="col-md-6 ">
                        @csrf
                        @if(isset($patient->id))
                        @method('PUT')
                        @endif
                        <div class="row">
                            <div class="form-group">
                                <label for="first_name">First Name:</label>
                                <input id="first_name" class="form-control " value="{{old('first_name', $patient->first_name)}}" type="text" name="first_name" placeholder="Juan" required>
                            </div>
                            <div class="form-group">
                                <label for="middle_name">Middle Name:</label>
                                <input id="middle_name" class="form-control" value="{{old('middle_name', $patient->middle_name)}}" type="text" name="middle_name" placeholder="P.">
                            </div>
                            <div class="form-group">
                                <label for="last_name">Last Name:</label>
                                <input id="last_name" class="form-control text-uppercase" value="{{old('last_name', $patient->last_name)}}" type="text" name="last_name" placeholder="Dela Cruz" required>
                            </div>
                            <div class="form-group">
                                <label for="suffix">Suffix:</label>
                                <input id="suffix" class="form-control" value="{{old('suffix', $patient->suffix)}}" type="text" name="suffix" placeholder="Jr.">
                            </div>
                            <div class="form-group">
                                <label for="birth_date">Date of Birth:</label>
                                <input id="birth_date" class="form-control" value="{{old('birth_date', $patient->birth_date)}}" type="date" name="birth_date" required>
                            </div>

                            <div class="form-group">
                                <label for="gender">Sex:</label>
                                <select id="gender" class="custom-select form-control" name="gender">
                                    <option value="M" {{old('gender', $patient->gender) === "M" ? 'selected' : ''}}>Male</option>
                                    <option value="F" {{old('gender', $patient->gender) === "F" ? 'selected' : ''}}>Female</option>
                                </select>
                            </div>

                        </div>
                        <p></p>

                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input id="email" class="form-control" value="{{old('email', $patient->email)}}" type="email" name="email" placeholder="12345@gmail.com" required>
                            </div>
                            <div class="form-group">
                                <label for="contact_no">Contact No.:</label>
                                <input id="contact_no" class="form-control" value="{{old('contact_no', $patient->contact_no)}}" type="number" name="contact_no" placeholder="09123456789" required>
                            </div>
                            <div class="form-group">
                                <label for="address">No. / Lot / Bldg. / Street:</label>
                                <input id="address" class="form-control" value="{{old('address', $patient->address)}}" type="text" name="address" placeholder="Lot 1 / Blk 1 Bataan Street" required>
                            </div>
                            <div class="form-group">
                                <label for="select_province">Province:</label>
                                <select id="select_province" class="form-control custom-select" name="province">
                                    <option value="">Select Province</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="select_citymun">City / Municipality:</label>
                                <select id="select_citymun" class="form-control custom-select" name="citymun">
                                    <option value="">Select City / Municipality</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="select_barangay">Barangay:</label>
                                <select id="select_barangay" class="form-control custom-select" name="barangay">
                                    <option value="">Select Barangay</option>
                                </select>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="row mt-3">
                    <div class="col-md col-sm-12">
                        <button class="btn btn-success" type="submit">Save</button>
                        <a href="{{route('patients.index')}}" class="btn btn-danger" type="button">Cancel</a>
                    </div>
                </div>
            </form>
            <!--//col-->
        </div>
        <!--//row-->

    </div>
    <!--//app-card-body-->

</div>
<!--//inner-->
</div>
@endsection

@section('js')
<script>
    'use strict'
    const patient = JSON.parse('{!! json_encode(isset($patient->id) ? $patient : null) !!}');

    let data = {
        provinces: [],
        citymuns: [],
        barangays: [],
        vaccines: []
    }
    const myModal = new bootstrap.Modal(document.getElementById('myModal'))
    const addNewVaxBtn = $('#addNewVaxBtn')
    addNewVaxBtn.click(function() {
        myModal.show()
    })
    const select_province = $('#select_province');
    const select_citymun = $('#select_citymun');
    const select_barangay = $('#select_barangay');
    const dose_no = $('#select_dose_no')
    const vaccination_date = $('#vaccination_date')
    const vaccine_name = $('#vaccine_name')
    const vaccination_facility = $('#vaccination_facility')
    const facility_region = $('#facility_region')
    const is_adverse_event = $('[name=is_adverse_event]')


    $.ajax({
        method: 'GET',
        url: '/js/json/refprovince.json',
        success: function(response) {
            data.provinces = response.RECORDS
            data.provinces.map(province => {
                $('#select_province').append($('<option>', {
                    value: province.provCode,
                    text: province.provDesc,
                    selected: patient ? province.provCode == patient.province ? true : false : ''
                }))
            })
            select_province.trigger('change')
        }
    })

    select_province.change(function(e) {
        const input = e.target.value
        select_citymun.empty()
        if (input != '') {
            $.ajax({
                method: 'GET',
                url: '/js/json/refcitymun.json',
                success: function(response) {
                    data.citymuns = response.RECORDS
                    data.citymuns.filter(citymun => {

                        if (citymun.provCode === input) {
                            select_citymun.append($('<option>', {
                                value: citymun.citymunCode,
                                text: citymun.citymunDesc,
                                selected: patient ? citymun.citymunCode == patient.citymun ? true : false : ''
                            }))
                        }

                    })
                    select_citymun.trigger('change')
                }
            })
        } else {
            select_citymun.val('')
            select_barangay.val('')
        }
    })
    select_province.trigger('change')
    select_citymun.change(function(e) {
        const input = e.target.value
        select_barangay.empty()
        if (input != '') {
            $.ajax({
                method: 'GET',
                url: '/js/json/refbrgy.json',
                success: function(response) {
                    data.barangays = response.RECORDS
                    data.barangays.filter(barangay => {
                        if (barangay.citymunCode == input) {
                            select_barangay.append($('<option>', {
                                value: barangay.brgyCode,
                                text: barangay.brgyDesc,
                                selected: patient ? barangay.brgyCode == patient.barangay ? true : false : ''
                            }))
                        }
                    })
                    select_barangay.removeAttr('disabled')
                }
            })
        } else {
            select_barangay.val('')
        }
    })
    select_citymun.trigger('change')
</script>
@endsection