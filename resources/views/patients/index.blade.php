@extends('layout.app')

@section('content')
<nav aria-label="Page breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Patients</li>
    </ol>
</nav>
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="my-modal-title">Vaccination Information</h5>

            </div>
            <div class="modal-body">
                <table class="table table-responsive table-hover" id="vax_table">
                    <thead class="thead-light">
                        <tr>
                            <th>Dose</th>
                            <th>Name</th>
                            <th>Facility</th>
                            <th>Vaccination_date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>

                </table>
            </div>

        </div>
    </div>
</div>
<a href="{{route('patients.create')}}" class="btn btn-success btn-sm" type="button">Add new</a>
<hr>
<table class="table table-light table-hover" id="patients_table">
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Age</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @forelse($patients as $patient)

        <tr>
            <td>{{$patient->id}}</td>
            <td>{{$patient->first_name}} {{$patient->middle_name ?? $patient->middle_name}} {{$patient->last_name}}</td>
            <td>{{$patient->gender === 'M' ? 'MALE' : 'FEMALE'}}</td>
            <td>{{\Carbon\Carbon::parse($patient->birth_date)->diff(\Carbon\Carbon::now())->y}} years old</td>
            <td>
                <form action="{{route('patients.destroy', $patient->id)}}" method="POST">
                    <a href="{{route('patients.index_vax', $patient->id)}}" class="btn btn-warning" type="button">View Vax</a>
                    <a href="{{route('patients.edit', $patient->id)}}" class="btn btn-info" type="button">Edit</a>
                    @csrf
                    @method("DELETE")
                    <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="5">No records found.</td>
        </tr>
        @endforelse
    </tbody>

</table>

@endsection

@section('js')
<script>
    const data = {
        selectedPatient: []
    }
    const myModal = new bootstrap.Modal(document.getElementById('myModal'))
    const vax_table = $('#vax_table')
    vax_table.DataTable({
        responsive: true,
        data: data.selectedPatient,
        columns: [{
                'data': 'dose_no'
            },
            {
                'data': 'vaccine_name'
            },
            {
                'data': 'vaccination_facility'
            },
            {
                'data': 'vaccination_date'
            },
            {
                'data': 'dose_no'
            },
        ]
    })

    function setPatient(patient) {
        data.selectedPatient = null
        data.selectedPatient = JSON.parse(patient)
        console.log(data.selectedPatient)
        myModal.show()
    }

    $(function() {
        $('#patients_table').DataTable();
    })
</script>
@endsection