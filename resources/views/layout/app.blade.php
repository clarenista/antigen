<!DOCTYPE html>
<html lang="en">

<head>
    <title>Antigen Testing</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" href="{{asset('/favicon.ico')}}">

    <!-- FontAwesome JS-->
    <script defer src="{{asset('assets/plugins/fontawesome/js/all.min.js')}}"></script>
    <script defer src="{{asset('assets/plugins/fontawesome/css/all.min.css')}}"></script>


    <!-- App CSS -->
    <link id="theme-style" rel="stylesheet" href="{{asset('assets/css/portal.css')}}">

    <!-- Datatables -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">


</head>

<body class="app">
    <?php
    $facility_details = \App\Models\Setting::where('item', 'facility_details')->orderBy('created_at', 'desc')->pluck('value')->first();
    $parsed = json_decode($facility_details, TRUE)
    ?>
    <header class="app-header fixed-top">
        <div class="app-header-inner">
            <div class="container-fluid py-2">
                <div class="app-header-content">
                    <div class="row justify-content-between align-items-center">

                        <div class="col-auto">
                            <a id="sidepanel-toggler" class="sidepanel-toggler d-inline-block d-xl-none" href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30" role="img">
                                    <title>Menu</title>
                                    <path stroke="currentColor" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2" d="M4 7h22M4 15h22M4 23h22"></path>
                                </svg>
                            </a>
                        </div>
                        <!--//col-->
                        <div class="search-mobile-trigger d-sm-none col">
                            <i class="search-mobile-trigger-icon fas fa-search"></i>
                        </div>
                        <!--//col-->


                        <div class="app-utilities col-auto">
                            <div class="app-utility-item app-user-dropdown dropdown">
                                <a class="dropdown-toggle" id="user-dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false"><img src="{{asset('/images/dp_default.png')}}" alt="user profile"></a>
                                <ul class="dropdown-menu" aria-labelledby="user-dropdown-toggle">
                                    <!-- <li><a class="dropdown-item" href="account.html">Account</a></li>
                                    <li><a class="dropdown-item" href="settings.html">Settings</a></li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li> -->
                                    <li>
                                        <form action="{{route('logout')}}" method="post">
                                            @csrf
                                            <button class="dropdown-item" type="submit">Log Out</button>
                                    </li>
                                    </form>
                                </ul>
                            </div>
                            <!--//app-user-dropdown-->
                        </div>
                        <!--//app-utilities-->
                    </div>
                    <!--//row-->
                </div>
                <!--//app-header-content-->
            </div>
            <!--//container-fluid-->
        </div>
        <!--//app-header-inner-->
        <div id="app-sidepanel" class="app-sidepanel sidepanel-hidden">
            <div id="sidepanel-drop" class="sidepanel-drop"></div>
            <div class="sidepanel-inner d-flex flex-column">
                <a href="#" id="sidepanel-close" class="sidepanel-close d-xl-none">&times;</a>
                <div class="app-branding">
                    <a class="app-logo" href="/"><img class="logo-icon me-2" src="{{asset('storage/' . $parsed['logo'])}}" alt="logo"><span class="logo-text">ANTIGEN TEST</span></a>

                </div>
                <!--//app-branding-->
                <nav id="app-nav-main" class="app-nav app-nav-main flex-grow-1">
                    <ul class="app-menu list-unstyled accordion" id="menu-accordion">
                        <li class="nav-item ">
                            <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                            <a class="nav-link" href="/">
                                <span class="nav-icon">
                                    <i class="fas fa-tachometer-alt fa-lg"></i>
                                </span>
                                <span class="nav-link-text">Overview</span>
                            </a>
                            <!--//nav-link-->
                        </li>
                        <!--//nav-item-->
                        <li class="nav-item">
                            <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                            <a class="nav-link" href="/lab-tests">
                                <span class="nav-icon">
                                    <i class="fas fa-vial fa-lg"></i>
                                </span>
                                <span class="nav-link-text">Lab Test Results</span>
                            </a>
                            <!--//nav-link-->
                        </li>
                        <!--//nav-item-->
                        <li class="nav-item">
                            <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                            <a class="nav-link" href="/patients">
                                <span class="nav-icon">
                                    <i class="fas fa-hospital-user fa-lg"></i>
                                </span>
                                <span class="nav-link-text">Patients</span>
                            </a>
                            <!--//nav-link-->
                        </li>
                        <!--//nav-item-->

                        <li class="nav-item">
                            <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                            <a class="nav-link" href="/physicians">
                                <span class="nav-icon">
                                    <i class="fas fa-user-md fa-lg"></i>
                                </span>
                                <span class="nav-link-text">Physicians</span>
                            </a>
                            <!--//nav-link-->
                        </li>
                        <!--//nav-item-->
                        @role('pathologist')
                        <li class="nav-item">
                            <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                            <a class="nav-link" href="/users">
                                <span class="nav-icon">
                                    <i class="fas fa-users fa-lg"></i>
                                </span>
                                <span class="nav-link-text">Users</span>
                            </a>
                            <!--//nav-link-->
                        </li>
                        <!--//nav-item-->




                        <li class="nav-item">
                            <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                            <a class="nav-link" href="/brands">
                                <span class="nav-icon">
                                    <i class="fas fa-copyright fa-lg"></i>
                                </span>
                                <span class="nav-link-text">Brands</span>
                            </a>
                            <!--//nav-link-->
                        </li>
                        <!--//nav-item-->
                        <li class="nav-item">
                            <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                            <a class="nav-link" href="/specimens">
                                <span class="nav-icon">
                                    <i class="fas fa-flask fa-lg"></i>
                                </span>
                                <span class="nav-link-text">Specimens</span>
                            </a>
                            <!--//nav-link-->
                        </li>
                        <!--//nav-item-->
                        <li class="nav-item">
                            <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                            <a class="nav-link" href="/tests">
                                <span class="nav-icon">
                                    <i class="fas fa-vials fa-lg"></i>
                                </span>
                                <span class="nav-link-text">Tests</span>
                            </a>
                            <!--//nav-link-->
                        </li>
                        <!--//nav-item-->
                        <!--//nav-item-->
                        <li class="nav-item">
                            <!--//Bootstrap Icons: https://icons.getbootstrap.com/ -->
                            <a class="nav-link" href="/settings">
                                <span class="nav-icon">
                                    <i class="fas fa-cog fa-lg"></i>
                                </span>
                                <span class="nav-link-text">Settings</span>
                            </a>
                            <!--//nav-link-->
                        </li>
                        <!--//nav-item-->
                        @endrole
                    </ul>
                    <!--//app-menu-->
                </nav>
                <!--//app-nav-->
            </div>
            <!--//sidepanel-inner-->
        </div>
        <!--//app-sidepanel-->
    </header>
    <!--//app-header-->

    <div class="app-wrapper" style="display: flex; flex-direction: column; height: 90vh">

        <div class="app-content pt-3 p-md-3 p-lg-4" style="display:flex; flex: 3 3;">
            <div class="container-xl">
                @yield('content')


            </div>
            <!--//container-fluid-->
        </div>
        <!--//app-content-->

        <footer class="app-footer" style="display:flex; flex: 0 1 1;">
            <div class=" container text-center py-3">
                <!--/* This template is free as long as you keep the footer attribution link. If you'd like to use the template without the attribution link, you can buy the commercial license via our website: themes.3rdwavemedia.com Thank you for your support. :) */-->
                <small class="copyright">{{ strtoupper($parsed['name'])}}</small>

            </div>
        </footer>
        <!--//app-footer-->

    </div>
    <!--//app-wrapper-->


    <!-- Javascript -->
    <script src="{{asset('assets/plugins/popper.min.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- Page Specific JS -->
    <script src="{{asset('assets/js/app.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.qrcode/1.0/jquery.qrcode.min.js"></script>

    @yield('js')
</body>

</html>