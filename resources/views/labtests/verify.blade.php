@extends('layout.public')

@section('content')

<div class="container" style="display: flex; height: 100%; justify-content: center; align-items: center; flex-direction: column">
    <div class="app-card shadow-sm mb-4 border-left-decoration">
        <div class="inner">
            <div class="app-card-body p-4">
                <div class="row gx-5 gy-3 mx-auto  ">
                    <!--//col-->
                    <p class="display-5 text-justify">
                        This is a valid <span class="text-uppercase {{$labtest->result === 'POSITIVE' ? 'text-danger' : 'text-success'}}"><strong>{{$labtest->result}}</strong></span> result of <span class="text-uppercase "><strong>{{$labtest->patient->name}}, {{$labtest->patient->age}}/{{$labtest->patient->gender}}</strong></span> done on <span class="text-uppercase "><strong>{{\Carbon\Carbon::parse($labtest->date)->toFormattedDateString() }} and {{\Carbon\Carbon::parse($labtest->time)->format('h:i:s A')}}</strong></span> at <span class="text-uppercase "><strong>{{$labtest->facility_details['name']}}</strong></span>. You may call {{$labtest->facility_details['contact']}} for further verification.
                    </p>
                </div>
                <!--//row-->

            </div>
            <!--//app-card-body-->

        </div>
        <!--//inner-->
    </div>
</div>


@endsection