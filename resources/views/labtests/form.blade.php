@extends('layout.app')

@section('content')
<nav aria-label="Page breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item" aria-current="page">Lab Tests</li>
        <li class="breadcrumb-item active" aria-current="page">Add new</li>
    </ol>
</nav>


<div id="myModal" class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div id="patient_content" style="display: none;">
                    <div id="patient_select">
                        <h3>Select Patient</h3>
                        <hr>
                        <table class="table table-light w-100" id="patient_table">
                            <thead class="thead-light">
                                <tr>
                                    <th>Name</th>
                                    <th>Age</th>
                                    <th>Action/s</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div id="addDetails" style="display: none;">
                        <div id="exposure_history">
                            <h3>Exposure History</h3>
                            <form action="">
                                <div class="form-group">
                                    <label for="is_health_worker">Health Worker:</label>
                                    <select id="is_health_worker" class="form-control custom-select" name="is_health_worker">
                                        <option value="">----</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                        <option value="2">Unknown</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="is_returning_overseas">Returning Overseas Filipino:</label>
                                    <select id="is_returning_overseas" class="form-control custom-select" name="is_returning_overseas">
                                        <option value="">----</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                        <option value="2">Unknown</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="history_exposure">History of exposure to known probable and/or confirmed COVID-19 case 14 days before the onset of signs and symptoms?:</label>
                                    <select id="history_exposure" class="form-control custom-select" name="history_exposure">
                                        <option value="">----</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                        <option value="2">Unknown</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="date_of_last_exposure"> Date of Last Exposure to Known Probable and/or Confirmed Case:</label>
                                    <input type="date" class="form-control" id="date_of_last_exposure" name="date_of_last_exposure" />
                                </div>
                                <div class="form-group">
                                    <label for="been_in_a_place">Have you been in a place with a Known COVID-19 community transmission 14 days before the onset of signs and symptoms?:</label>
                                    <select id="been_in_a_place" class="form-control custom-select" name="been_in_a_place">
                                        <option value="">----</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                        <option value="2">Unknown</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="place_of_exposure">Place of Exposure</label>
                                    <textarea id="place_of_exposure" class="form-control h-100" name="place_of_exposure" rows="5"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="travel_date_from">Travel dates (from):</label>
                                    <input type="date" class="form-control" id="travel_date_from" name="travel_date_from" />
                                </div>
                                <div class="form-group mb-3">
                                    <label for="travel_date_to">Travel dates (to):</label>
                                    <input type="date" class="form-control" id="travel_date_to" name="travel_date_to" />
                                </div>
                                <button class="btn btn-primary float-end" type="button" id="exposureHistoryBtnNext">Next</button>
                            </form>
                        </div>
                        <div id="symptoms" style="display: none;">
                            <h3>Signs and Symptoms</h3>
                            <form action="">
                                <div class="form-group">
                                    <label for="fever">Fever:</label>
                                    <select id="fever" class="form-control custom-select" name="fever">
                                        <option value="">----</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="cough">Cough:</label>
                                    <select id="cough" class="form-control custom-select" name="cough">
                                        <option value="">----</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="fatigue">General Weakness / Fatigue:</label>
                                    <select id="fatigue" class="form-control custom-select" name="fatigue">
                                        <option value="">----</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="headache">Headache:</label>
                                    <select id="headache" class="form-control custom-select" name="headache">
                                        <option value="">----</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="myalgia">Myalgia:</label>
                                    <select id="myalgia" class="form-control custom-select" name="myalgia">
                                        <option value="">----</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="sore_throat">Sore Throat:</label>
                                    <select id="sore_throat" class="form-control custom-select" name="sore_throat">
                                        <option value="">----</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="coryza">Coryza:</label>
                                    <select id="coryza" class="form-control custom-select" name="coryza">
                                        <option value="">----</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="nausea">Anorexia / Nausea / Vomiting:</label>
                                    <select id="nausea" class="form-control custom-select" name="nausea">
                                        <option value="">----</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="diarrhea">Diarrhea:</label>
                                    <select id="diarrhea" class="form-control custom-select" name="diarrhea">
                                        <option value="">----</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="altered_mental_status">Altered Mental Status:</label>
                                    <select id="altered_mental_status" class="form-control custom-select" name="altered_mental_status">
                                        <option value="">----</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="difficulty_of_breathing">Difficulty of Breathing / Shortness of Breath:</label>
                                    <select id="difficulty_of_breathing" class="form-control custom-select" name="difficulty_of_breathing">
                                        <option value="">----</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="anosmia">Anosmia (Loss of Smell):</label>
                                    <select id="anosmia" class="form-control custom-select" name="anosmia">
                                        <option value="">----</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="ageusia">Ageusia (Loss of Taste):</label>
                                    <select id="ageusia" class="form-control custom-select" name="ageusia">
                                        <option value="">----</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="place_of_exposure">Other Signs and Symptoms (excluding those specified):</label>
                                    <textarea id="other_symptoms" class="form-control h-100" name="other_symptoms" rows="5"></textarea>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="onset_of_illness">Date of Onset of Illness:</label>
                                    <input type="date" class="form-control" id="onset_of_illness" name="onset_of_illness" />
                                </div>
                                <button class="btn btn-primary float-end" type="button" id="symptomsBtnNext">Next</button>
                            </form>
                        </div>
                        <div id="xray" style="display: none;">
                            <h3>X-ray Results</h3>
                            <form action="">
                                <div class="form-group">
                                    <label for="is_done">Chest X-ray Done?</label>
                                    <select id="is_done" class="form-control custom-select" name="is_done">
                                        <option value="">----</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div id="xray_details">
                                    <div class="form-group">
                                        <label for="xray_date">Date when Chest X-ray was Done:</label>
                                        <input type="date" class="form-control" id="xray_date" name="xray_date" />
                                    </div>

                                    <div class="form-group">
                                        <label for="xray_result">Chest X-Ray Results:</label>
                                        <input type="text" class="form-control" id="xray_result" name="xray_result" />
                                    </div>
                                    <div class="form-group">
                                        <label for="others_xray_result"> Others, please specify:</label>
                                        <input type="text" class="form-control" id="others_xray_result" name="others_xray_result" />
                                    </div>
                                    <div class="form-group">
                                        <label for="ct_result">Chest CT Results:</label>
                                        <input type="text" class="form-control" id="ct_result" name="ct_result" />
                                    </div>
                                    <div class="form-group">
                                        <label for="others_ct_result"> Others, please specify:</label>
                                        <input type="text" class="form-control" id="others_ct_result" name="others_ct_result" />
                                    </div>
                                    <div class="form-group">
                                        <label for="lung_result">Chest CT Results:</label>
                                        <input type="text" class="form-control" id="lung_result" name="lung_result" />
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="others_lung_result"> Others, please specify:</label>
                                        <input type="text" class="form-control" id="others_lung_result" name="others_lung_result" />
                                    </div>
                                </div>

                                <button class="btn btn-primary float-end" type="button" id="xrayBtnNext">Next</button>
                            </form>
                        </div>
                        <div id="1labresult" style="display: none;">
                            <h3>Laboratory Results (<span id="labTimes"></span>)</h3>
                            <form action="">
                                <div class="form-group">
                                    <label for="specimen_collection_date">Date Specimen Collected:</label>
                                    <input type="date" class="form-control" id="specimen_collection_date" name="specimen_collection_date" />
                                </div>
                                <div class="form-group">
                                    <label for="specimen_received_date">Date Specimen Received by Laboratory:</label>
                                    <input type="date" class="form-control" id="specimen_received_date" name="specimen_received_date" />
                                </div>
                                <div class="form-group">
                                    <label for="test_type">Type of Test:</label>
                                    <select id="test_type" class="form-control custom-select" name="test_type">
                                        <option value="">----</option>
                                        @foreach($testtypes as $type)
                                        <option value="{{$type->id}}" data-description="{{$type->description}}">{{$type->description}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="kit_brand">Brand of Kit:</label>
                                    <input type="text" class="form-control" id="kit_brand" name="kit_brand" />
                                </div>
                                <div class="form-group">
                                    <label for="reason">Reason for Antigen Testing:</label>
                                    <textarea id="reason" class="form-control h-100" name="reason" rows="3"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="result_date">Date of Release of Result:</label>
                                    <input type="date" class="form-control" id="result_date" name="result_date" />
                                </div>
                                <div class="form-group">
                                    <label for="result">Test Result:</label>
                                    <input type="text" class="form-control" id="result" name="result" />
                                </div>
                                <div class="form-group mb-3">
                                    <label for="testing_lab">Lab where Testing was Done / Health Facility:</label>
                                    <input type="text" class="form-control" id="testing_lab" name="testing_lab" />
                                </div>
                                <button class="btn btn-primary float-end" type="button" id="labResultBtnNext">Next</button>
                            </form>
                        </div>
                        <div id="healthStatus" style="display: none;">
                            <h3>Outcome and Health Status Details</h3>
                            <form action="">

                                <div class="form-group">
                                    <label for="status">Health Status:</label>
                                    <select id="status" class="form-control custom-select" name="status">
                                        <option value="">----</option>
                                        @foreach($healthStatusTypes as $type)
                                        <option value="{{$type->id}}">{{$type->description}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="disposition">Disposition of the Case:</label>
                                    <select id="disposition" class="form-control custom-select" name="disposition">
                                        <option value="">----</option>
                                        @foreach($dispositionTypes as $type)
                                        <option value="{{$type->id}}">{{$type->description}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="admitted_date">Date Admitted/Isolated/Discharge:</label>
                                    <input type="date" class="form-control" id="admitted_date" name="admitted_date" />
                                </div>
                                <div class="form-group">
                                    <label for="facility_province_first_admitted"> Province of Facility where patient was first admitted:</label>
                                    <input type="text" class="form-control" id="facility_province_first_admitted" name="facility_province_first_admitted" />
                                </div>
                                <div class="form-group">
                                    <label for="facility_name"> Name of Facility where patient was first admitted:</label>
                                    <input type="text" class="form-control" id="facility_name" name="facility_name" />
                                </div>
                                <div class="form-group">
                                    <label for="outcome">Outcome:</label>
                                    <select id="outcome" class="form-control custom-select" name="outcome">
                                        <option value="">----</option>
                                        <option value="Active">Active</option>
                                        <option value="Died">Died</option>
                                        <option value="Recovered">Recovered</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="recovery_date">Date Recovered:</label>
                                    <input type="date" class="form-control" id="recovery_date" name="recovery_date" />
                                </div>
                                <div class="form-group">
                                    <label for="die_date">Date Died:</label>
                                    <input type="date" class="form-control" id="die_date" name="die_date" />
                                </div>
                                <div class="form-group">
                                    <label for="cause_of_death"> Cause of Death:</label>
                                    <input type="text" class="form-control" id="cause_of_death" name="cause_of_death" />
                                </div>
                                <div class="form-group">
                                    <label for="classification">Classification:</label>
                                    <select id="classification" class="form-control custom-select" name="classification">
                                        <option value="">----</option>
                                        <option value="Close Contact">Close Contact</option>
                                        <option value="Suspect">Suspect</option>
                                        <option value="Probable">Probable</option>
                                        <option value="Others">Others</option>
                                    </select>
                                </div>
                                <button class="btn btn-primary float-end" type="button" id="submitDetails">Submit</button>
                            </form>
                        </div>
                    </div>
                    <div id="patient_add_new" style="display: none;">


                        <!-- <form method="post" id="patient_form">

                            <div class="row">
                                <div class="form-group">
                                    <label for="first_name">First Name:</label>
                                    <input id="first_name" class="form-control" type="text" name="first_name" placeholder="Juan" required>
                                </div>
                                <div class="form-group">
                                    <label for="middle_name">Middle Name:</label>
                                    <input id="middle_name" class="form-control" type="text" name="middle_name" placeholder="P.">
                                </div>
                                <div class="form-group">
                                    <label for="last_name">Last Name:</label>
                                    <input id="last_name" class="form-control" type="text" name="last_name" placeholder="Dela Cruz" required>
                                </div>
                                <div class="form-group">
                                    <label for="birth_date">Date of Birth:</label>
                                    <input id="birth_date" class="form-control" type="date" name="birth_date" required>
                                </div>

                                <div class="form-group">
                                    <label for="gender">Sex:</label>
                                    <select id="gender" class="custom-select form-control" name="gender">
                                        <option value="M">Male</option>
                                        <option value="F">Female</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email:</label>
                                    <input id="email" class="form-control" type="email" name="email" placeholder="12345@gmail.com" required>
                                </div>
                            </div>
                            <p></p>
                            <button class="btn btn-success" type="submit">Save</button>
                            <a href="#" class="btn btn-danger" type="button">Cancel</a>
                        </form> -->
                    </div>
                </div>
                <div id="physician_content" style="display: none;">
                    <div id="patient_select">
                        <h3>Select Requesting Physician</h3>
                        <button class="btn btn-warning" type="button" onclick="handleNoPhysician()">No Physician</button>
                        <hr>
                        <table class="table table-light w-100" id="physician_table">
                            <thead class="thead-light">
                                <tr>
                                    <th>Name</th>
                                    <th>Date Added</th>
                                    <th>Action/s</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div id="patient_add_new" style="display: none;">

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="app-card shadow-sm mb-4 border-left-decoration">
    <div class="inner">
        <div class="app-card-body p-4">
            <div class="row gx-5 gy-3">
                <div class="col-md-4 ">
                    <form action="{{isset($labtest->id) ? route('labTests.update', $labtest->id) :route('labTests.store')}}" method="post">
                        @csrf
                        @if(isset($labtest->id))
                        @method('PUT')
                        @endif
                        <form method="POST" action="">
                            @csrf
                            <div class="form-group">
                                <label for="patient">Date and Time</label>
                                <input type="datetime-local" class="form-control" name="dateTime">

                            </div>
                            <div class="form-group">
                                <label for="patient">Patient</label>
                                <input type="text" class="form-control" readonly name="patient">

                            </div>
                            <div class="form-group">
                                <label for="patient">Requesting Physician</label>
                                <input type="text" class="form-control" readonly name="physician">
                            </div>

                            <div class="form-group" style="display: none;">
                                <label for="test">Test</label>
                                <select id="test" class="custom-select form-control" name="select_test">
                                    @foreach($tests as $test)
                                    <option value="{{$test->id}}" data-details="{{$test}}">{{$test->description}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="test">Result</label>
                                <select id="test" class="custom-select form-control" name="select_result">
                                    <option value="">--SELECT RESULT--</option>
                                    <option value="NEG">NEGATIVE</option>
                                    <option value="POS">POSITIVE</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="test">Brand</label>
                                <select id="test" class="custom-select form-control" name="select_brand">
                                    <option value="">--SELECT BRAND--</option>
                                    @foreach($brands as $brand)
                                    <option value="{{$brand->id}}" data-details="{{$brand}}">{{$brand->name}} - {{$brand->manufacturer}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group" style="display: none;">
                                <label for="test">Specimen</label>
                                <select id="test" class="custom-select form-control" name="select_specimen">
                                    @foreach($specimens as $specimen)
                                    <option value="{{$specimen->id}}" data-details="{{$specimen}}">{{$specimen->description}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </form>
                        <p></p>
                        <button class="btn btn-success" id="submitBtn" type="submit" onclick="saveLabTest()"><span id="submitBtnLabel">Save</span></button>
                        <a href="{{route('labTests.index')}}" class="btn btn-danger" type="button">Cancel</a>
                    </form>
                </div>
                <div class="col-md">
                    <div class="row">
                        <div class="col">
                            <strong>NAME: <span class="text-uppercase" id="last_name"></span>,</strong> <span id="first_name"></span>, <span id="middle_name"></span>
                        </div>
                        <div class="col">
                            <strong>AGE/SEX: </strong> <span id="age"></span> y.o/ <span id="sex"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <strong>DATE OF BIRTH: </strong> <span class="" id="birth_date"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <strong>REQUESTING PHYSICIAN: </strong> <span class="text-uppercase" id="requesting_physician"></span>
                        </div>
                    </div>
                    <div class="row my-3 text-center">
                        <div class="col">
                            <strong>LABORATORY TEST RESULT </strong>
                        </div>
                    </div>
                    <div class="row text-center">
                        <div class="col">
                            <p>
                                <strong>TEST </strong>
                            </p>
                            <p id="show_test_type" class="lead">

                            </p>
                        </div>
                        <div class="col">
                            <p>
                                <strong>RESULT </strong>
                            </p>
                            <p id="show_result" class="lead">

                            </p>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-3">
                            <strong>Test Information: </strong>
                        </div>
                        <div class="col">
                            <p>Novel Coronavirus (2019-nCoV) Antigen Detection Kit <br>
                                Qualitative determination of 2019-nCoV Antigen<br>
                                (Lateral Flow Method)

                            </p>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-3">
                            <strong>Brand: </strong>
                        </div>
                        <div class="col">
                            <p id="brand_name"></p>
                            <p id="brand_manufacturer"></p>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-3">
                            <strong>SPECIMEN: </strong>
                        </div>
                        <div class="col">
                            <p id="specimen"></p>
                        </div>
                    </div>
                </div>
                <!--//col-->
            </div>
            <!--//row-->

        </div>
        <!--//app-card-body-->

    </div>
    <!--//inner-->
</div>
@endsection

@section('js')
<script>
    'use strict'

    function handleNoPhysician() {
        data.physician = {
            id: 0
        }
        $("[name=physician]").val('')
        $('#requesting_physician').html('')
        $('#myModal').toggle()
        $('.fade').hide()
    }

    let data = {
        dateTime: '',
        patient: [],
        physician: {
            id: 0
        },
        test: [],
        result: '',
        brand: [],
        specimen: [],
        exposure_history: [],
        symptoms: [],
        xray: [],
        healthStatus: [],
        labTestResults: []
    }
    var myModal = new bootstrap.Modal(document.getElementById('myModal'))

    const specimen_collection_date = $('#specimen_collection_date')
    const specimen_received_date = $('#specimen_received_date')
    const test_type = $('#test_type')
    const kit_brand = $('#kit_brand')
    const reason = $('#reason')
    const result_date = $('#result_date')
    const result = $('#result')
    const testing_lab = $('#testing_lab')

    const is_health_worker = $("#is_health_worker")
    const is_returning_overseas = $("#is_returning_overseas")
    const history_exposure = $("#history_exposure")
    const date_of_last_exposure = $("#date_of_last_exposure")
    const been_in_a_place = $("#been_in_a_place")
    const place_of_exposure = $("#place_of_exposure")
    const travel_date_from = $("#travel_date_from")
    const travel_date_to = $("#travel_date_to")

    const fever = $("#fever")
    const cough = $("#cough")
    const fatigue = $("#fatigue")
    const headache = $("#headache")
    const myalgia = $("#myalgia")
    const sore_throat = $("#sore_throat")
    const coryza = $("#coryza")
    const nausea = $("#nausea")
    const diarrhea = $("#diarrhea")
    const altered_mental_status = $("#altered_mental_status")
    const difficulty_of_breathing = $("#difficulty_of_breathing")
    const anosmia = $("#anosmia")
    const ageusia = $("#ageusia")
    const other_symptoms = $("#other_symptoms")
    const onset_of_illness = $("#onset_of_illness")

    const addElementValue = (value, ...el) => {
        el.forEach(item => {
            item.val(value)
        })
    }
    const is_done = $("#is_done")
    const xray_details = $("#xray_details")
    const xray_date = $("#xray_date")
    const xray_result = $("#xray_result")
    const others_xray_result = $("#others_xray_result")
    const ct_result = $("#ct_result")
    const others_ct_result = $("#others_ct_result")
    const lung_result = $("#lung_result")
    const others_lung_result = $("#others_lung_result")
    is_done.change(function(e) {
        const {
            target
        } = e
        if (target.value === '0') {
            xray_details.hide()
            addElementValue(null, xray_date, xray_result, others_xray_result, ct_result, others_ct_result, lung_result, others_lung_result)
        } else {
            xray_details.show()


        }
    })

    const status = $("#status")
    const disposition = $("#disposition")
    const admitted_date = $("#admitted_date")
    const facility_province_first_admitted = $("#facility_province_first_admitted")
    const facility_name = $("#facility_name")
    const outcome = $("#outcome")
    const recovery_date = $("#recovery_date")
    const die_date = $("#die_date")
    const cause_of_death = $("#cause_of_death")
    const classification = $("#classification")


    $("[name=dateTime]").change(function(e) {
        data.dateTime = $(this).val()
    })

    $("[name=patient]").click(function() {
        myModal.show()
        $('#patient_content').show()
        $('#physician_content').hide()
        $('.fade').show()

        clearLabResultFormInputs('exposure_history')
        clearLabResultFormInputs('symptoms')
        clearLabResultFormInputs('xray')
        clearLabResultFormInputs('1labresult')
        clearLabResultFormInputs('healthStatus')

        $('#healthStatus').hide()
        $('#patient_select').show()
        // $('#patient_content').show()

    })
    $("[name=physician]").click(function() {
        myModal.show()
        $('#patient_content').hide()
        $('#physician_content').show()
        $('.fade').show()
    })

    $('#patient_table').DataTable({
        "ajax": "/api/patients",
        "columns": [{
                "data": "id",
                render: (id, type, row, meta) => {
                    return row.first_name + " " + row.last_name
                }
            },
            {
                "data": "created_at",
                render: (id, type, row, meta) => {
                    return getAge(row.birth_date) + " y.o"
                }
            },
            {
                "data": "created_at",
                render: (id, type, row, meta) => {
                    const stringify = JSON.stringify(row)

                    return `<button class="btn btn-sm btn-info" onclick='setPatient(${stringify})'>Select</button>`
                }
            },

        ],
        "language": {
            "zeroRecords": `<button class='btn btn-success btn-sm' onclick='addNewPatient()'>Add new</button>`,
        }
    })

    $('#physician_table').DataTable({
        "ajax": "/api/physicians",
        "columns": [{
                "data": "id",
                render: (id, type, row, meta) => {
                    return row.first_name + " " + row.last_name + " " + row.suffix
                }
            },
            {
                "data": "created_at"
            },
            {
                "data": "created_at",
                render: (id, type, row, meta) => {
                    const stringify = JSON.stringify(row)

                    return `<button class="btn btn-sm btn-info" onclick='setPhysician(${stringify})'>Select</button>`
                }
            },

        ],
        "language": {
            "zeroRecords": `<button class='btn btn-success btn-sm' onclick='addNewPhysician()'>Add new</button>`,
        }
    })

    $('[name=select_test]').change(function(e) {
        const details = $(this).find(':selected').data('details')
        data.test = details
        $('#show_test_type').html(data.test.description)
    }).trigger('change')
    $('[name=select_result]').change(function(e) {
        const res = e.target.value
        data.result = res
        $('#show_result').html(data.result === 'NEG' ? 'NEGATIVE' : 'POSITIVE')

    })

    $('[name=select_brand]').change(function(e) {
        const details = $(this).find(':selected').data('details')
        data.brand = details
        $('#brand_name').html(data.brand.name)
        $('#brand_manufacturer').html(data.brand.manufacturer)

    })
    $('[name=select_specimen]').change(function(e) {
        const details = $(this).find(':selected').data('details')
        data.specimen = details
        $('#specimen').html(data.specimen.description)

    }).trigger('change')

    function setPatient(res) {
        data.patient = res
        $('#first_name').html(data.patient.first_name)
        $('#middle_name').html(data.patient.middle_name)
        $('#last_name').html(data.patient.last_name)
        $('#age').html(getAge(data.patient.birth_date))
        $('#sex').html(data.patient.gender)
        $('#birth_date').html(data.patient.birth_date)
        $("[name=patient]").val(data.patient.first_name + " " + data.patient.last_name)
        $('#patient_select').hide()
        $('#addDetails').show()
        $('#exposure_history').show()
        // $('#myModal').toggle()
        // $('.fade').hide()
    }

    function setPhysician(res) {
        data.physician = res
        $("[name=physician]").val(data.physician.first_name + " " + data.physician.last_name + " " + data.physician.suffix)
        $('#requesting_physician').html(data.physician.first_name + " " + data.physician.last_name + ", " + data.physician.suffix)
        $('#myModal').toggle()
        $('.fade').hide()
    }




    function addNewPatient() {
        $('#patient_select').hide()
        $('#patient_add_new').show()
    }

    function saveLabTest() {
        const [date, time] = (data.dateTime).split('T')
        $('#submitBtn').attr('disabled', 'disabled')
        $('#submitBtnLabel').text('Please wait...')
        const fd = new FormData()
        fd.append('date', date)
        fd.append('time', time)
        fd.append('patient_id', data.patient.id)
        fd.append('email_address', data.patient.email)
        fd.append('requesting_physician_id', data.physician.id)
        fd.append('test_id', data.test.id)
        fd.append('result', data.result)
        // fd.append('test_information', data.patient.email)
        fd.append('brand_id', data.brand.id)
        fd.append('specimen_id', data.specimen.id)
        fd.append('medtech_id', '{{Auth::id()}}')
        fd.append('exposure_history', JSON.stringify(data.exposure_history))
        fd.append('symptoms', JSON.stringify(data.symptoms))
        fd.append('xray', JSON.stringify(data.xray))
        fd.append('healthStatus', JSON.stringify(data.healthStatus))
        fd.append('labTestResults', JSON.stringify(data.labTestResults))
        const api = '/api/lab-tests/store'
        $.ajax({
            method: 'post',
            processData: false,
            contentType: false,
            cache: false,
            data: fd,
            enctype: 'multipart/form-data',
            url: api,
            success: function(response) {
                if (response.status === 'ok') {
                    window.location.href = '/lab-tests'
                }
            },
            error: function(err) {
                $('#submitBtnLabel').text('Save')
                $('#submitBtn').removeAttr('disabled')
            }
        });

    }

    function getAge(dateString) {
        var ageInMilliseconds = new Date() - new Date(dateString);
        return Math.floor(ageInMilliseconds / 1000 / 60 / 60 / 24 / 365); // convert to years
    }

    function renderSex(val) {
        if (val === 'M') {
            return "MALE"
        } else {
            return "FEMALE"

        }
    }

    $('#exposureHistoryBtnNext').click(function() {
        const newExposureHistory = new ExposureHistory()
        if (!newExposureHistory.hasBlank()) {

            data.exposure_history = newExposureHistory
            // hide exposure history form
            $('#exposure_history').hide()
            // show symptoms form
            $('#symptoms').show()
        }
    })
    $('#symptomsBtnNext').click(function() {
        // const inputs = $('#symptoms > form :input ')

        const newSymptoms = new Symptoms()
        if (!newSymptoms.hasBlank()) {

            data.symptoms = newSymptoms
            // hide symptoms  form
            $('#symptoms').hide()
            // show xray form
            $('#xray').show()
        }
    })
    $('#xrayBtnNext').click(function() {

        const newXray = new Xray()
        if (!newXray.hasBlank()) {

            data.xray = newXray
            // hide xray  form
            $('#xray').hide()
            // show 1labresult form
            $('#1labresult').show()
        }

    })
    // span#labTimes update text
    $('#labTimes').text(data.labTestResults.length + 1)
    $('#labResultBtnNext').click(function() {
        const newLabResult = new LabResult()
        data.labTestResults.push(newLabResult)
        if (data.labTestResults.length <= 1) {
            // data.labTestResults.push(newLabResult)
            $('#labTimes').text(data.labTestResults.length + 1)
            clearLabResultFormInputs('1labresult')

        } else {
            // hide 1labresult  form
            $('#1labresult').hide()
            // show healthStatus form
            $('#healthStatus').show()


        }
    });

    function clearLabResultFormInputs(id) {
        const labInputs = $(`#${id} > form :input`).val('')
    }

    $('#submitDetails').click(function(e) {
        e.preventDefault()
        const newHealthStatus = new HealthStatus()
        data.healthStatus = newHealthStatus
        myModal.toggle()
        // if (!newHealthStatus.hasBlank()) {

        // }

    })
    const checkIfBlank = (...el) => {
        const len = el.length
        let hasError = []
        el.forEach(item => {

            if (item.val() === '') {
                item.css({
                    border: '3px solid red'
                })
                hasError.push(item)

                // bool = true
            } else {
                item.css({
                    border: '2px solid #eee'
                })

                // bool = false
            }

        })
        if (hasError <= len) return false
        else return true
        // return bool
    }
    class ExposureHistory {

        constructor() {
            this.patient_id = data.patient.id
            this.is_health_worker = is_health_worker.val()
            this.is_returning_overseas = is_returning_overseas.val()
            this.history_exposure = history_exposure.val()
            this.date_of_last_exposure = date_of_last_exposure.val() ? date_of_last_exposure.val() : null
            this.been_in_a_place = been_in_a_place.val()
            this.place_of_exposure = place_of_exposure.val()
            this.travel_date_from = travel_date_from.val() ? travel_date_from.val() : null
            this.travel_date_to = travel_date_to.val() ? travel_date_to.val() : null
        }

        hasBlank() {
            return checkIfBlank(is_health_worker, is_returning_overseas, history_exposure, been_in_a_place, place_of_exposure)
        }
    }


    class Symptoms {
        constructor() {
            this.patient_id = data.patient.id
            this.fever = fever.val()
            this.cough = cough.val()
            this.fatigue = fatigue.val()
            this.headache = headache.val()
            this.myalgia = myalgia.val()
            this.sore_throat = sore_throat.val()
            this.coryza = coryza.val()
            this.nausea = nausea.val()
            this.diarrhea = diarrhea.val()
            this.altered_mental_status = altered_mental_status.val()
            this.difficulty_of_breathing = difficulty_of_breathing.val()
            this.anosmia = anosmia.val()
            this.ageusia = ageusia.val()
            this.other_symptoms = other_symptoms.val()
            this.onset_of_illness = onset_of_illness.val() ? onset_of_illness.val() : null
        }
        hasBlank() {
            return checkIfBlank(
                fever, cough, fatigue, headache, myalgia, sore_throat, coryza, nausea, diarrhea, altered_mental_status, difficulty_of_breathing, anosmia, ageusia, other_symptoms)
        }
    }

    class Xray {
        constructor() {
            this.patient_id = data.patient.id
            this.is_done = is_done.val()
            this.xray_date = xray_date.val() ? xray_date.val() : null
            this.xray_result = xray_result.val()
            this.others_xray_result = others_xray_result.val()
            this.ct_result = ct_result.val()
            this.others_ct_result = others_ct_result.val()
            this.lung_result = lung_result.val()
            this.others_lung_result = others_lung_result.val()
        }
        hasBlank() {
            if (this.is_done === '1') {

                return checkIfBlank(is_done, xray_date, xray_result, others_xray_result, ct_result, others_ct_result, lung_result, others_lung_result)
            }
            return checkIfBlank(is_done)
        }

    }

    class HealthStatus {
        constructor() {
            this.patient_id = data.patient.id
            this.health_status_type_id = status.val() ? status.val() : null
            this.disposition_type_id = disposition.val() ? disposition.val() : null
            this.admitted_date = admitted_date.val() ? admitted_date.val() : null
            this.facility_province_first_admitted = facility_province_first_admitted.val()
            this.facility_name = facility_name.val()
            this.outcome = outcome.val()
            this.recovery_date = recovery_date.val() ? recovery_date.val() : null
            this.die_date = die_date.val() ? die_date.val() : null
            this.cause_of_death = cause_of_death.val()
            this.classification = classification.val()
        }
        hasBlank() {
            return checkIfBlank(facility_province_first_admitted, facility_name, outcome, classification)
        }

    }

    class LabResult {

        constructor() {
            this.patient_id = data.patient.id
            this.specimen_collection_date = specimen_collection_date.val() ? specimen_collection_date.val() : null
            this.specimen_received_date = specimen_received_date.val() ? specimen_received_date.val() : null
            this.test_type_id = Number(test_type.val())
            this.kit_brand = kit_brand.val()
            this.reason = reason.val()
            this.result_date = result_date.val() ? result_date.val() : null
            this.result = result.val()
            this.testing_lab = testing_lab.val()
        }
        // hasBlank() {
        //     return checkIfBlank(specimen_collection_date, specimen_received_date, test_type, kit_brand, reason, result_date, result, testing_lab)
        // }
    }
</script>
@endsection