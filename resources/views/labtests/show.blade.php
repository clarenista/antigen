@extends('layout.app')

@section('content')
<nav aria-label="Page breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item" aria-current="page"><a href="{{route('labTests.index')}}">Lab Tests</a></li>
        <li class="breadcrumb-item active" aria-current="page">View Test</li>
    </ol>
</nav>

<div id="myModal" class="modal fade" aria-labelledby="my-modal-title" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div id="patient_content" style="display: none;">
                    <div id="patient_select">
                        <h3>Select Patient</h3>
                        <hr>
                        <table class="table table-light w-100" id="patient_table">
                            <thead class="thead-light">
                                <tr>
                                    <th>Name</th>
                                    <th>Age</th>
                                    <th>Action/s</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div id="patient_add_new" style="display: none;">
                        <!-- <form method="post" id="patient_form">

                            <div class="row">
                                <div class="form-group">
                                    <label for="first_name">First Name:</label>
                                    <input id="first_name" class="form-control" type="text" name="first_name" placeholder="Juan" required>
                                </div>
                                <div class="form-group">
                                    <label for="middle_name">Middle Name:</label>
                                    <input id="middle_name" class="form-control" type="text" name="middle_name" placeholder="P.">
                                </div>
                                <div class="form-group">
                                    <label for="last_name">Last Name:</label>
                                    <input id="last_name" class="form-control" type="text" name="last_name" placeholder="Dela Cruz" required>
                                </div>
                                <div class="form-group">
                                    <label for="birth_date">Date of Birth:</label>
                                    <input id="birth_date" class="form-control" type="date" name="birth_date" required>
                                </div>

                                <div class="form-group">
                                    <label for="gender">Sex:</label>
                                    <select id="gender" class="custom-select form-control" name="gender">
                                        <option value="M">Male</option>
                                        <option value="F">Female</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email:</label>
                                    <input id="email" class="form-control" type="email" name="email" placeholder="12345@gmail.com" required>
                                </div>
                            </div>
                            <p></p>
                            <button class="btn btn-success" type="submit">Save</button>
                            <a href="#" class="btn btn-danger" type="button">Cancel</a>
                        </form> -->
                    </div>
                </div>
                <div id="physician_content" style="display: none;">
                    <div id="patient_select">
                        <h3>Select Requesting Physician</h3>
                        <hr>
                        <table class="table table-light w-100" id="physician_table">
                            <thead class="thead-light">
                                <tr>
                                    <th>Name</th>
                                    <th>Date Added</th>
                                    <th>Action/s</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div id="patient_add_new" style="display: none;">

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="app-card shadow-sm mb-4 border-left-decoration">
    <div class="inner">
        <div class="app-card-body p-4">
            <div class="row gx-5 gy-3">
                <div class="col-md">
                    <div class="row">
                        <div class="col">
                            <strong>NAME: <span class="text-uppercase" id="last_name"></span>,</strong> <span id="first_name"></span>, <span id="middle_name"></span>
                        </div>
                        <div class="col">
                            <strong>AGE/SEX: </strong> <span id="age"></span> y.o/ <span id="sex"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <strong>DATE OF BIRTH: </strong> <span class="" id="birth_date"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <strong>REQUESTING PHYSICIAN: </strong> <span class="text-uppercase" id="requesting_physician"></span>
                        </div>
                    </div>
                    <div class="row my-3 text-center">
                        <div class="col">
                            <strong>LABORATORY TEST RESULT </strong>
                        </div>
                    </div>
                    <div class="row text-center">
                        <div class="col">
                            <p>
                                <strong>TEST </strong>
                            </p>
                            <p id="test_type" class="lead">

                            </p>
                        </div>
                        <div class="col">
                            <p>
                                <strong>RESULT </strong>
                            </p>
                            <p id="result" class="lead">

                            </p>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-3">
                            <strong>Test Information: </strong>
                        </div>
                        <div class="col">
                            <p>Novel Coronavirus (2019-nCoV) Antigen Detection Kit <br>
                                Qualitative determination of 2019-nCoV Antigen<br>
                                (Lateral Flow Method)

                            </p>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-3">
                            <strong>Brand: </strong>
                        </div>
                        <div class="col">
                            <p id="brand_name"></p>
                            <p id="brand_manufacturer"></p>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-3">
                            <strong>SPECIMEN: </strong>
                        </div>
                        <div class="col">
                            <p id="specimen"></p>
                        </div>
                    </div>

                    <div class="row ">
                        <div class="col-3">
                            <strong>TEST KIT PHOTO: </strong>
                        </div>
                        <div class="col">

                            <div class="accordion" id="accordionExample">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingQrCode">
                                        <button class="accordion-button " type="button" data-bs-toggle="collapse" data-bs-target="#collapseQrCode" aria-expanded="true" aria-controls="collapseQrCode">
                                            QR CODE
                                        </button>
                                    </h2>
                                    <div id="collapseQrCode" class="accordion-collapse collapse show" aria-labelledby="headingQrCode" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <div id="qrcode"></div>
                                        </div>
                                    </div>
                                </div>
                                @if($labtest->uploaded_path)

                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingUploaded">
                                        <button class="accordion-button " type="button" data-bs-toggle="collapse" data-bs-target="#collapseUploaded" aria-expanded="true" aria-controls="collapseUploaded">
                                            UPLOADED FILE/S
                                        </button>
                                    </h2>
                                    <div id="collapseUploaded" class="accordion-collapse collapse show" aria-labelledby="headingUploaded" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            @foreach($labtest['images'] as $image)
                                            <img src="{{asset('storage/'.$image)}}" class="img-fluid" style="border: 2px solid black ; margin: 10px" />
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-3">
                            @role('medTech')
                            @if($labtest->for_validation === 0)

                            <form method="POST" action="{{route('labTests.to_validate', $labtest->id)}}">
                                @csrf
                                @method('PUT')
                                <button class="btn btn-warning" type="submit">Submit for validation</button>
                            </form>
                            @else
                            <button class="btn btn-info" type="button">Submitted for validation</button>
                            @endif
                            @endrole
                            @role('pathologist')
                            @if($labtest->for_validation != 0)
                            @if($labtest->approved_by == null)
                            <form method="POST" action="{{route('labTests.approve', $labtest->id)}}">
                                @csrf
                                @method('PUT')
                                <button class="btn btn-primary" type="submit">Mark as Approve</button>
                            </form>
                            @else
                            <button class="btn btn-info" type="button">Approved</button>
                            @endif
                            @else
                            <p class="lead text-danger"><i>For medtech action</i></p>
                            @endif
                            @endrole
                        </div>
                    </div>
                </div>
                <!--//col-->
            </div>
            <!--//row-->

        </div>
        <!--//app-card-body-->

    </div>
    <!--//inner-->
</div>
@endsection

@section('js')
<script>
    'use strict'

    let data = {
        patient: [],
        physician: [],
        test: [],
        result: '',
        brand: [],
        specimen: [],
        key: ''
    }

    const getLabTestApi = '/api/lab-tests/{{$labtest->id}}'
    $.ajax({
        method: 'get',
        processData: false,
        contentType: false,
        cache: false,
        enctype: 'multipart/form-data',
        url: getLabTestApi,
        success: function(response) {
            data.test = response.data.test
            data.brand = response.data.brand
            data.specimen = response.data.specimen
            data.result = response.data.result
            data.key = response.data.key
            setPatient(response.data.patient)
            setPhysician(response.data.physician)
            setField('test_type', data.test.description)
            setField('result', data.result === 'NEG' ? 'NEGATIVE' : 'POSITIVE')
            setField('brand_name', data.brand.name)
            setField('brand_manufacturer', data.brand.manufacturer)
            setField('specimen', data.specimen.description)
            setInput('[name=select_test]', data.test.id)
            setInput('[name=select_result]', data.result)
            $('#qrcode').qrcode(`http://192.168.0.16/api/lab-tests/${data.key}/upload`);
            // $('#qrcode').qrcode("{{route('apiUpload', ['key' =>" + data.key + "])}}");
            // setInput('[name=select_result]', data.result)








        }
    });


    var myModal = new bootstrap.Modal(document.getElementById('myModal'))

    $("[name=patient]").click(function() {
        myModal.show()
        $('#patient_content').show()
        $('#physician_content').hide()
        $('.fade').show()
    })
    $("[name=physician]").click(function() {
        myModal.show()
        $('#patient_content').hide()
        $('#physician_content').show()
        $('.fade').show()
    })

    $('#patient_table').DataTable({
        "ajax": "/api/patients",
        "columns": [{
                "data": "id",
                render: (id, type, row, meta) => {
                    return row.first_name + " " + row.last_name
                }
            },
            {
                "data": "created_at",
                render: (id, type, row, meta) => {
                    return getAge(row.birth_date) + " y.o"
                }
            },
            {
                "data": "created_at",
                render: (id, type, row, meta) => {
                    const stringify = JSON.stringify(row)

                    return `<button class="btn btn-sm btn-info" onclick='setPatient(${stringify})'>Select</button>`
                }
            },

        ],
        "language": {
            "zeroRecords": `<button class='btn btn-success btn-sm' onclick='addNewPatient()'>Add new</button>`,
        }
    })

    $('#physician_table').DataTable({
        "ajax": "/api/physicians",
        "columns": [{
                "data": "id",
                render: (id, type, row, meta) => {
                    return row.first_name + " " + row.last_name + " " + row.suffix
                }
            },
            {
                "data": "created_at"
            },
            {
                "data": "created_at",
                render: (id, type, row, meta) => {
                    const stringify = JSON.stringify(row)

                    return `<button class="btn btn-sm btn-info" onclick='setPhysician(${stringify})'>Select</button>`
                }
            },

        ],
        "language": {
            "zeroRecords": `<button class='btn btn-success btn-sm' onclick='addNewPhysician()'>Add new</button>`,
        }
    })

    function setField(selector, data) {
        $(`#${selector}`).html(data)
    }

    function setInput(selector, data) {
        $(`${selector}`).val(data)

    }

    $('[name=select_test]').change(function(e) {
        const details = $(this).find(':selected').data('details')
        console.log(details)
        const res = JSON.parse(e.target.value)
        data.test = res
        // $('#test_type').html(data.test.description)
        setField('test_type', data.test.description)

    })
    $('[name=select_result]').change(function(e) {
        const res = e.target.value
        data.result = res
        // $('#result').html(data.result === 'NEG' ? 'NEGATIVE' : 'POSITIVE')
        setField('result', data.result === 'NEG' ? 'NEGATIVE' : 'POSITIVE')

    })

    $('[name=select_brand]').change(function(e) {
        const res = JSON.parse(e.target.value)
        data.brand = res
        $('#brand_name').html(data.brand.name)
        // $('#brand_manufacturer').html(data.brand.manufacturer)
        setField('brand_name', data.brand.name)
        setField('brand_manufacturer', data.brand.manufacturer)

    })
    $('[name=select_specimen]').change(function(e) {
        const res = JSON.parse(e.target.value)
        data.specimen = res
        $('#specimen').html(data.specimen.description)
        setField('specimen', data.specimen.description)

    })

    function setPatient(res) {
        data.patient = res
        $('#first_name').html(data.patient.first_name)
        $('#middle_name').html(data.patient.middle_name)
        $('#last_name').html(data.patient.last_name)
        $('#age').html(getAge(data.patient.birth_date))
        $('#sex').html(data.patient.gender)
        $('#birth_date').html(data.patient.birth_date)
        $("[name=patient]").val(data.patient.first_name + " " + data.patient.last_name)
        $('#myModal').toggle()
        $('.fade').hide()
    }

    function setPhysician(res) {
        // data.physician = res
        // $("[name=physician]").val(data.physician.first_name + " " + data.physician.last_name + " " + data.physician.suffix)
        // $('#requesting_physician').html(data.physician.first_name + " " + data.physician.last_name + ", " + data.physician.suffix)
        // $('#myModal').toggle()
        // $('.fade').hide()
    }

    function addNewPatient() {
        $('#patient_select').hide()
        $('#patient_add_new').show()
    }

    function saveLabTest() {
        const fd = new FormData()
        fd.append('patient_id', data.patient.id)
        fd.append('email_address', data.patient.email)
        fd.append('requesting_physician_id', data.physician.id)
        fd.append('test_id', data.test.id)
        fd.append('result', data.result)
        // fd.append('test_information', data.patient.email)
        fd.append('brand_id', data.brand.id)
        fd.append('speciment_id', data.specimen.id)
        fd.append('medtech_id', '{{Auth::id()}}')
        // console.log('{{Auth::id()}}')
        const api = '/api/lab-tests/store'
        $.ajax({
            method: 'post',
            processData: false,
            contentType: false,
            cache: false,
            data: fd,
            enctype: 'multipart/form-data',
            url: api,
            success: function(response) {
                if (response.status === 'ok') {
                    window.location.href = '/lab-tests'
                }
            }
        });

    }

    function getAge(dateString) {
        var ageInMilliseconds = new Date() - new Date(dateString);
        return Math.floor(ageInMilliseconds / 1000 / 60 / 60 / 24 / 365); // convert to years
    }

    function renderSex(val) {
        if (val === 'M') {
            return "MALE"
        } else {
            return "FEMALE"

        }
    }
</script>
@endsection