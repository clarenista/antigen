@extends('layout.app')

@section('content')
<nav aria-label="Page breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Lab Tests</li>
    </ol>
</nav>
@if (session('status'))
<div class="alert alert-success">
    {{ session('status') }}
</div>
@endif
@role('pathologist')
<p class="text-{{$auto_approve != 0 ? 'success' : 'danger'}}"><small>Auto Approved: <strong>{{$auto_approve != 0 ? 'Activated' : 'Deactivated'}}</strong></small></p>
@endrole
<a href="{{route('labTests.create')}}" class="btn btn-success btn-sm" type="button">Add new</a>
<button class="btn btn-secondary btn-sm float-end" type="button" id="exportBtn">Export</button>
<hr>

<table class="table table-light table-hover" id="labtests_table">
    <thead class="thead-light">
        <tr>
            <th>
                <div class="form-check">
                    <!-- <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault"> -->
                </div>
            </th>
            <th>Date Added</th>
            <th>Patient</th>
            <th>Requesting Physician</th>
            <th>Result</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @forelse($labtests as $test)
        <tr>
            <td>
                <div class="form-check d-flex justify-content-center">
                    <input class="select-checkbox" type="checkbox" value="{{$test->id}}" id="{{$test->id}}" name="checkbox_labtests">
                </div>
            </td>
            <td>{{$test->created_at}}</td>
            <td>{{$test->patient->first_name ?? ''}} {{$test->patient->last_name ?? ''}}</td>
            @if($test->requesting_physician_id)
            <td>{{$test->physician->first_name}} {{$test->physician->last_name}}, {{$test->physician->suffix}}</td>
            @else
            <td>---</td>
            @endif
            <td>{{$test->result === 'NEG' ? 'NEGATIVE' : 'POSTIVE' }}</td>
            <td>
                <a href="{{route('labTests.show', $test->id)}}" class="btn btn-info" type="button">View</a>
                @if($test->approved_by != null)
                <a href="{{route('pdf', $test->key)}}" target="_blank" class="btn btn-success" type="button">PDF</a>
                <a href="{{route('labTests.mail_patient', $test->key)}}" class="btn btn-warning" type="button">Send email</a>
                @endif
            </td>

        </tr>
        @empty
        @endforelse
    </tbody>

</table>


@endsection

@section('js')
<script>
    const flexSwitchCheckAutoApprove = $('#flexSwitchCheckAutoApprove')
    flexSwitchCheckAutoApprove.click(function() {
        const confirm = confirm('You are about to activate the Auto Approval feature. Are you sure?')
    })
    const labtests_table = $('#labtests_table').DataTable({
        columnDefs: [{
            orderable: false,
            // className: 'select-checkbox',
            targets: 0,
            // 'checkboxes': {
            //     'selectRow': true
            // }
        }],
        order: [
            [1, 'asc']
        ]
    })
    $('#exportBtn').click(function() {

        var labtests = [];
        $.each($("[name=checkbox_labtests]:checked"), function() {
            labtests.push($(this).val());
        });
        if (labtests.length <= 0) {
            alert('Exporting requires at least one lab test. Please select a lab test.')
        } else {

            $.ajax({
                url: '/api/reports',
                method: 'POST',
                data: {
                    'labtests_ids': labtests
                },
                success: function(res) {
                    var a = document.createElement("a");
                    a.href = res.file;
                    a.download = res.name;
                    document.body.appendChild(a);
                    a.click();
                    a.remove();
                }
            })
        }
    })
</script>
@endsection