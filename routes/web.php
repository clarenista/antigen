<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\LabTestResultController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PhysicianController;
use App\Http\Controllers\ResultController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\SpecimenController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SettingController;

// Route::get('/mail/{id}', [LabTestResultController::class, 'mail_test'])->name('mail_test');


Route::middleware(['auth'])->group(function () {
    Route::get('/', function () {
        return view('layout.app');
    });


    Route::group(['middleware' => ['role:pathologist']], function () {
        Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {
            Route::get('/', [SettingController::class, 'index'])->name('index');
            Route::post('/update-facility', [SettingController::class, 'updateFacility'])->name('updateFacility');
            Route::post('/update-logo', [SettingController::class, 'updateLogo'])->name('updateLogo');
            Route::post('/update-auto-approve', [SettingController::class, 'updateAutoApprove'])->name('updateAutoApprove');
        });
        Route::resource('/users', UserController::class);
        Route::resource('/results', ResultController::class);
        Route::resource('/brands', BrandController::class);
        Route::resource('/specimens', SpecimenController::class);
        Route::resource('/tests', TestController::class);

        Route::group(['prefix' => 'lab-tests', 'as' => 'labTests.'], function () {
            Route::get('/', [LabTestResultController::class, 'index'])->name('index');
            Route::put('/{id}/approve', [LabTestResultController::class, 'approve'])->name('approve');
        });
    });
    Route::group(['middleware' => ['role:pathologist|medTech']], function () {
        Route::resource('/patients', PatientController::class);
        Route::group(['prefix' => 'patients', 'as' => 'patients.'], function () {
            Route::get('/{id}/vaccinations/', [PatientController::class, 'index_vax'])->name('index_vax');
            Route::get('/{id}/vaccinations/create', [PatientController::class, 'create_vax'])->name('create_vax');
            Route::post('/{id}/vaccinations/store', [PatientController::class, 'store_vax'])->name('store_vax');
            Route::get('/{id}/vaccinations/{vax_id}/edit', [PatientController::class, 'edit_vax'])->name('edit_vax');
            Route::put('/{id}/vaccinations/{vax_id}/update', [PatientController::class, 'update_vax'])->name('update_vax');
        });
        Route::resource('/physicians', PhysicianController::class);
        Route::group(['prefix' => 'lab-tests', 'as' => 'labTests.'], function () {
            Route::get('/', [LabTestResultController::class, 'index'])->name('index');
            Route::get('/{id}/show', [LabTestResultController::class, 'show'])->name('show');
            Route::get('/{id}/mail', [LabTestResultController::class, 'mail_patient'])->name('mail_patient');
            Route::put('/{id}/to-validate', [LabTestResultController::class, 'to_validate'])->name('to_validate');
        });
    });

    Route::group(['middleware' => ['role:medTech']], function () {

        Route::group(['prefix' => 'lab-tests', 'as' => 'labTests.'], function () {

            Route::get('/create', [LabTestResultController::class, 'create'])->name('create');
            Route::post('/store', [LabTestResultController::class, 'store'])->name('store');
            Route::get('/{id}/edit', [LabTestResultController::class, 'edit'])->name('edit');
            Route::put('/{id}/update', [LabTestResultController::class, 'update'])->name('update');
            Route::delete('/{id}/delete', [LabTestResultController::class, 'delete'])->name('delete');
        });
    });
});

Route::get('/{key}/pdf', [LabTestResultController::class, 'pdf'])->name('pdf');
Route::get('lab-tests/{key}/verify', [LabTestResultController::class, 'verify'])->name('verify');

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);
