<?php

use App\Http\Controllers\Api\LabTestResultController;
use App\Http\Controllers\Api\PatientController;
use App\Http\Controllers\Api\PhysicianController;
use App\Http\Controllers\Api\ReportController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/patients', [PatientController::class, 'getPatients']);
Route::get('/patients/store', [PatientController::class, 'store']);

Route::get('/physicians', [PhysicianController::class, 'getPhysicians']);

Route::post('/lab-tests/store', [LabTestResultController::class, 'store']);
Route::get('/lab-tests/{id}', [LabTestResultController::class, 'getLabTest']);
Route::get('/lab-tests/{key}/check-key', [LabTestResultController::class, 'checkKey']);
Route::post('/lab-tests/{key}/upload', [LabTestResultController::class, 'upload'])->name('apiUpload');
Route::post('/lab-tests/{key}/saveUpload', [LabTestResultController::class, 'saveUpload'])->name('saveUpload');
Route::post('/reports', [ReportController::class, 'getReports']);
