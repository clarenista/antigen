<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LabTestResultMail extends Mailable
{
    use Queueable, SerializesModels;


    public $labTestResult;
    public $root;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($labTestResult)
    {
        $this->labTestResult = $labTestResult;
        $this->root = base_path();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('labtest@bobongmd.com', 'LabTest')
            ->subject('COVID-19 RAPID ANTIGEN TEST REPORT')
            ->with(['patientName' => $this->labTestResult->patient->first_name . ' ' . $this->labTestResult->patient->last_name, 'root' => $this->root])
            ->view('emails.labtestresult');
    }
}
