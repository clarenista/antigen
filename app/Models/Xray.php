<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Xray extends Model
{
    use HasFactory;
    protected $fillable = [
        'patient_id',
        'lab_test_result_id',
        'is_done',
        'xray_date',
        'result',
        'others_xray_result',
        'ct_result',
        'others_ct_result',
        'lung_result',
        'others_lung_result'
    ];
}
