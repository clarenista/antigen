<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vaccination extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'patient_id',
        'vaccination_date',
        'vaccine_name',
        'vaccination_facility',
        'facility_region',
        'is_adverse_event',
        'dose_no'
    ];
}
