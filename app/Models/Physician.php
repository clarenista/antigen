<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Physician extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['user_id', 'first_name', 'middle_name', 'last_name', 'suffix', 'specialty', 'prc_no', 'signature'];
}
