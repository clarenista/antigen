<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MedTech extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'first_name', 'middle_name', 'last_name', 'suffix', 'license_no', 'signature', 'position'];
}
