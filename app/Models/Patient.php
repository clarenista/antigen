<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Patient extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'gender',
        'email',
        'birth_date',
        'contact_no',
        'suffix',
        'nationality',
        'address',
        'province',
        'citymun',
        'barangay'

    ];

    function vaccinations()
    {
        return $this->hasMany('App\Models\Vaccination', 'patient_id', 'id');
    }

    function province_details()
    {
        return $this->hasOne('App\Models\Province', 'provCode', 'province');
    }
    function citymun_details()
    {
        return $this->hasOne('App\Models\CityMun', 'citymunCode', 'citymun');
    }
    function barangay_details()
    {
        return $this->hasOne('App\Models\Barangay', 'brgyCode', 'barangay');
    }
}
