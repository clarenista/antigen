<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HealthStatus extends Model
{
    use HasFactory;

    protected $fillable = [
        'patient_id',
        'lab_test_result_id',
        'health_status_type_id',
        'disposition_type_id',
        'admitted_date',
        'facility_province_first_admitted',
        'facility_name',
        'outcome',
        'recovery_date',
        'die_date',
        'cause_of_death',
        'classification'
    ];

    public function health_status_type()
    {
        return $this->hasOne('App\Models\HealthStatusType', 'id', 'health_status_type_id');
    }

    public function disposition_type()
    {

        return $this->hasOne('App\Models\DispositionType', 'id', 'disposition_type_id');
    }
}
