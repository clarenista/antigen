<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LabTestResult extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'date',
        'time',
        'date_time',
        'patient_id',
        // 'address',
        'email_address',
        'requesting_physician_id',
        'test_id',
        'result',
        'test_information',
        'brand_id',
        'specimen_id',
        'medtech_id',
        'comment',
        'isEmailed',
        'labTestResults',
        'approved_by',
        'for_validation',
        'verified_by',
        'verified_at',
        'key',
        'uploaded_path'
    ];

    public function patient()
    {
        return $this->hasOne('App\Models\Patient', 'id', 'patient_id');
    }

    public function physician()
    {

        return $this->hasOne('App\Models\Physician', 'id', 'requesting_physician_id');
    }

    public function medtech()
    {

        return $this->hasOne('App\Models\User', 'id', 'medtech_id');
    }


    public function approved_by_details()
    {

        return $this->hasOne('App\Models\User', 'id', 'approved_by');
    }


    public function test()
    {

        return $this->hasOne('App\Models\Test', 'id', 'test_id');
    }

    public function brand()
    {

        return $this->hasOne('App\Models\Brand', 'id', 'brand_id');
    }

    public function specimen()
    {

        return $this->hasOne('App\Models\Specimen', 'id', 'specimen_id');
    }

    public function exposure_history()
    {
        return $this->hasOne('App\Models\PatientExposureHistory', 'lab_test_result_id', 'id');
    }

    public function symptoms()
    {
        return $this->hasOne('App\Models\PatientSymptom', 'lab_test_result_id', 'id');
    }
    public function xray()
    {
        return $this->hasOne('App\Models\Xray', 'lab_test_result_id', 'id');
    }
    public function healthStatus()
    {
        return $this->hasOne('App\Models\HealthStatus', 'lab_test_result_id', 'id');
    }
    // public function labTestResults()
    // {
    //     return $this->hasOne('App\Models\PatientExposureHistory', 'lab_test_result_id', 'id');
    // }
}
