<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RLabtest extends Model
{
    use HasFactory;

    public function lab_test_type()
    {
        return $this->hasOne('App\Models\LabTestType', 'id', 'test_type_id');
    }
}
