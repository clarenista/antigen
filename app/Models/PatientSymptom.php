<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientSymptom extends Model
{
    use HasFactory;

    protected $fillable = [
        'patient_id',
        'lab_test_result_id',
        'fever',
        'cough',
        'fatigue',
        'headache',
        'myalgia',
        'sore_throat',
        'coryza',
        'nausea',
        'diarrhea',
        'altered_mental_status',
        'difficulty_of_breathing',
        'anosmia',
        'ageusia',
        'other_symptoms',
        'onset_of_illness'
    ];
}
