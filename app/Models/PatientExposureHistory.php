<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientExposureHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'patient_id',
        'lab_test_result_id',
        'is_health_worker',
        'is_returning_overseas',
        'history_exposure',
        'date_of_last_exposure',
        'been_in_a_place',
        'place_of_exposure',
        'travel_date_from',
        'travel_date_to'
    ];
}
