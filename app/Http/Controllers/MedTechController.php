<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MedTech;

class MedTechController extends Controller
{
    public function index()
    {
        $medteches = MedTech::all();
        return view('medteches.index', compact('medteches'));
    }

    public function create()
    {
        $MedTech = new MedTech();
        return view('medteches.form', compact('MedTech'));
    }

    public function store(Request $request)
    {
        MedTech::create($request->except('_token'));
        return redirect(\route('medteches.index'));
    }

    public function edit(MedTech $MedTech)
    {
        return view('medteches.form', compact('MedTech'));
    }

    public function update(Request $request, MedTech $MedTech)
    {
        $MedTech->update($request->except('_token'));
        return redirect(\route('medteches.index'));
    }

    public function destroy(MedTech $MedTech)
    {
        MedTech::destroy($MedTech->id);
        return redirect(\route('medteches.index'));
    }
}
