<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LabTestResult;
use App\Models\Setting;
use Carbon\Carbon;

class LabTestResultController extends Controller
{
    protected string $auto_approve;
    function __construct()
    {
        $this->auto_approve = Setting::where('item', 'auto_approve')->orderBy('created_at', 'desc')->pluck('value')->first();
    }
    public function store(Request $request)
    {
        $exposure_history = json_decode($request->exposure_history, true);
        // return $exposure_history;
        $symptoms = json_decode($request->symptoms, true);
        $xray = json_decode($request->xray, true);
        $healthStatus = json_decode($request->healthStatus, true);
        $labTestResults = $request->labTestResults;
        // return $labTestResults;
        // if ($this->auto_approve != 0) {
        //     $request->merge(['approved_by' => 1]);
        // }
        $key = md5(\Carbon\Carbon::now());
        $request->merge(['labTestResults' => $labTestResults, 'key' => $key]);
        // PatientExposureHistory::create($exposure_history);
        $labTest = LabTestResult::create($request->except('_token'));
        $labTest->exposure_history()->create($exposure_history);
        $labTest->symptoms()->create($symptoms);
        $labTest->xray()->create($xray);
        $labTest->healthStatus()->create($healthStatus);
        return response()->json(['status' => 'ok']);
    }

    public function getLabTest($id)
    {
        $labTest = LabTestResult::with('patient', 'physician', 'brand', 'specimen', 'test')->find($id);
        return response()->json(['data' => $labTest]);
    }

    public function upload($key)
    {
        $labTest = LabTestResult::where('key', $key)->first();
        return response()->json(['data' => $labTest]);
    }
    public function checkKey($key)
    {
        $labTest = LabTestResult::where('key', $key)->first();
        if ($labTest->uploaded_path) {
            return response()->json(['status' => 'failed', 'message' => 'Lab Test image already exists. ']);
        }
        return response()->json(['data' => $labTest]);
    }
    public function saveUpload($key)
    {
        // return \request()->all();
        $labTest = LabTestResult::where('key', $key)->first();
        $path = \request()->file('file')->store('uploaded');
        $allPath = $labTest->uploaded_path . $path . ',';
        $labTest->update(['uploaded_path' => $allPath]);
        return response()->json(['status' => 'ok', 'message' => 'success']);
    }
}
