<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LabTestResult;
use App\Models\LabTestType;
use App\Exports\LabResultsExport;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    public function getReports()
    {
        // $labtest = LabTestResult::with('patient.vaccinations', 'patient.province_details', 'patient.citymun_details', 'patient.barangay_details', 'exposure_history', 'symptoms', 'xray', 'healthStatus.health_status_type', 'healthStatus.disposition_type')->whereIn('id', \request()->labtests_ids)->get();
        $now = \Carbon\Carbon::now()->format('Ymd_H:i:s');
        $name = $now . '_Antigen-Linelist.xlsx';
        $excel = Excel::raw(new LabResultsExport(\request()->labtests_ids), \Maatwebsite\Excel\Excel::XLSX);
        $response =  array(
            'name' => $name, //no extention needed
            'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($excel) //mime type of used format
        );
        return response()->json($response);
    }
}
