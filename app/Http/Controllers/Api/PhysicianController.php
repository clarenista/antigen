<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Physician;
use Illuminate\Http\Request;

class PhysicianController extends Controller
{
    public function getPhysicians(Request $request)
    {
        // return \request()->query();

        $data = Physician::all();

        return response()->json(['data' => $data]);

        // return response()->json(['data' =>  ]);
    }
}
