<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PatientController extends Controller
{
    public function getPatients(Request $request)
    {
        // return \request()->query();

        $data = Patient::all();

        return response()->json(['data' => $data]);

        // return response()->json(['data' =>  ]);
    }

    public function store(Request $request)
    {
    }
}
