<?php

namespace App\Http\Controllers;

use App\Models\Test;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index(Test $test)
    {
        $tests = Test::all();
        return view('tests.index', compact('tests', 'test'));
    }

    public function store(Request $request)
    {
        Test::create($request->except('_token'));
        return redirect(\route('tests.index'));
    }
    public function edit(Test $test)
    {
        $tests = Test::all();
        return view('tests.index', compact('tests', 'test'));
    }


    public function update(Request $request, Test $test)
    {
        $test->update($request->except('_token'));
        return redirect(\route('tests.index'));
    }
    public function destroy(Test $test)
    {
        Test::destroy($test->id);
        return redirect(\route('test.index'));
    }
}
