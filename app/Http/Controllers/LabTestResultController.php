<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LabTestResult;
use App\Models\Test;
use App\Models\Brand;
use App\Models\Specimen;
use App\Models\LabTestType;
use App\Models\HealthStatusType;
use App\Models\DispositionType;
use App\Models\Setting;
use PDF;
use Carbon\Carbon;
use App\Mail\LabTestResultMail;
use Illuminate\Support\Facades\Mail;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use \setasign\Fpdi\Fpdi;


class LabTestResultController extends Controller
{
    protected string $auto_approve;
    function __construct()
    {
        $this->auto_approve = Setting::where('item', 'auto_approve')->orderBy('created_at', 'desc')->pluck('value')->first();
    }
    public function index()
    {
        $labtests = LabTestResult::with('patient', 'physician')->get();
        $auto_approve = $this->auto_approve;
        return view('labtests.index', compact('labtests', 'auto_approve'));
    }

    public function create(LabTestResult $labtest)
    {

        $tests = Test::all();
        $brands = Brand::all();
        $specimens = Specimen::all();
        $testtypes = LabTestType::all();
        $healthStatusTypes = HealthStatusType::all();
        $dispositionTypes = DispositionType::all();
        return view('labtests.form', compact('labtest', 'tests', 'brands', 'specimens', 'testtypes', 'healthStatusTypes', 'dispositionTypes'));
    }

    public function show($id)
    {
        $tests = Test::all();
        $brands = Brand::all();
        $specimens = Specimen::all();
        $labtest = LabTestResult::with('patient', 'physician', 'brand', 'specimen')->find($id);
        $labtest['images'] = explode(',', $labtest->uploaded_path);
        return view('labtests.show', compact('labtest', 'tests', 'brands', 'specimens'));
    }

    public function store(Request $request)
    {

        LabTestResult::create($request->all());
        return redirect(\route('labtests.index'));
    }

    public function pdf($key, Request $request)
    {
        $labtest = LabTestResult::with('patient', 'physician', 'brand', 'specimen', 'medtech', 'approved_by_details')->where('key', $key)->first();
        $medSign = \asset('storage/' . $labtest->medtech->signature);
        $physician = $labtest->requesting_physician_id ? $labtest->physician->first_name . ' ' . $labtest->physician->middle_name . ' ' . $labtest->physician->last_name . ', ' . $labtest->physician->suffix : ' --- ';

        $facility = Setting::where('item', 'facility_details')->orderBy('created_at', 'desc')->first();
        $parsed = json_decode($facility->value, TRUE);

        // Logo
        $logo = \asset('storage/' . $parsed['logo']);
        // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)
        $qr = QrCode::format('png')->size(300)->generate(\route('verify', ['key' => $key]));
        $qrbase64 = ('data:image/png;base64,' . base64_encode($qr));

        $img = explode(',', $qrbase64, 2)[1];
        $pic = 'data://text/plain;base64,' . $img;

        $pdf = new FPDI('p', 'pt', 'Letter');
        $pdf->setSourceFile('docs/blank.pdf');
        // Import the first page from the PDF and add to dynamic PDF
        $tpl = $pdf->importPage(1);
        // $pdf->SetLeftMargin(400);
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 0);

        // Use the imported page as the template
        $pdf->useTemplate($tpl);


        // adding a Cell using:
        // $pdf->Cell( $width, $height, $text, $border, $fill, $align);

        // Name
        $pdf->SetFont('Helvetica', 'B', 30);
        // $pdf->SetTextColor(0,0,0);
        $pdf->SetXY(20, 315); // set the position of the box
        $pdf->Cell(0, 10, utf8_decode('$name'), 0, 0, 'C'); // add the text, align to Center of cell

        // Serial number
        // $pdf->SetTextColor(255, 255, 255);
        // $pdf->SetXY(664, 587);
        // $pdf->Cell(1, 10, '$serial_no', 0, 0, "L");
        // Image(string file [, float x [, float y [, float w [, float h [, string type [, mixed link]]]]]])

        $pdf->Image($logo, 80, 6, 70, 70, 'PNG');
        // $pdf->MultiCell(130, 5, strtoupper($parsed['name']), 0, 'C', 0, 0, 55, 7, true);
        $pdf->SetFont('Times', '', 16);
        $pdf->SetXY(150, 7);
        $pdf->MultiCell(350, 20, strtoupper(utf8_decode($parsed['name'])), 0, 'C');
        $pdf->SetFont('Times', '', 8);
        $pdf->SetXY(150, 46);
        $pdf->MultiCell(350, 20, strtoupper(utf8_decode($parsed['address'])), 0, 'C');
        $pdf->SetXY(150, 59);
        $pdf->MultiCell(350, 20, strtoupper(utf8_decode($parsed['contact'])), 0, 'C');
        $pdf->Line(560, 90, 50, 90);
        $pdf->Image($pic, 10, 350, 150, 150, 'png');
        $pdf->Output();

        // TCPDF
        PDF::setHeaderCallback(function ($pdf) use ($medSign, $labtest, $key) {
            // $pdf::Image('@' . $imgdata, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // Medtech Sign
            // $pdf->Image($qr, 40, 250, 18, 12, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
            // PYT Sign
            $pdf->Image($medSign, 40, 250, 18, 12, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
            // check if approved by Patho
            if ($labtest->approved_by) {
                $pathoSign = \asset('storage/' . $labtest->approved_by_details->signature);
                $pdf->Image($pathoSign, 114, 245, 35, 25, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
            }

            $facility = Setting::where('item', 'facility_details')->orderBy('created_at', 'desc')->first();
            $parsed = json_decode($facility->value, TRUE);

            // Logo
            $logo = \asset('storage/' . $parsed['logo']);
            // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)
            $pdf->Image($logo, 20, 6, 30, 30, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
            // $pdf->Image($logo, 100, 6, 28, 30, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);

            // Set font
            $pdf->SetFont('freeserif', '', 16);

            // set some text for example
            $txt = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
            // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
            $pdf->Ln(10);
            // $pdf->MultiCell(300, 30, strtoupper($parsed['name']), 0, 'L', 0, 1);
            // $pdf->SetFillColor(255, 255, 127);
            $pdf->MultiCell(130, 5, strtoupper($parsed['name']), 0, 'C', 0, 0, 55, 7, true);
            $pdf->SetFont('freeserif', '', 8);
            $pdf->MultiCell(130, 5, $parsed['address'], 0, 'C', 0, 0, 55, 23, true);
            $pdf->MultiCell(130, 5, $parsed['contact'], 0, 'C', 0, 0, 55, 28, true);
            // $pdf->Cell(0, 0, $parsed['address'], 0, 1, 'C', 0, '', 0);
            // $pdf->Cell(0, 0, 'Tel Nos.: ' . $parsed['address'], 0, 1, 'C', 0, '', 0);
            // $pdf->Cell(0, 0, 'Tel Nos.: ' . $parsed['contact'], 0, 1, 'C', 0, '', 0);
            $pdf->Ln(10);
            $pdf->writeHTML("<hr>", true, false, false, false, '');
        });
        $tagvs = array('p' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n'
        => 0)));
        PDF::setHtmlVSpace($tagvs);
        PDF::SetMargins(25, 10, 25);
        $html = '
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>

            <table >
                <tr>
                    <td><strong>DATE:</strong> ' . Carbon::parse($labtest->date)->toFormattedDateString() . '</td>
                    <td style="text-align:right"><strong>TIME:</strong> ' . substr($labtest->time, 0, 5)  . ' hours</td>
                </tr>
            </table>
            <br>
            <br>
            <br>
            <table>
                <tr >
                    <td><strong>NAME: ' . $labtest->patient->last_name . '</strong>, ' . $labtest->patient->first_name . ', ' . $labtest->patient->middle_name . '</td>
                    <td style="text-align:right"><strong>AGE/SEX:</strong>' . Carbon::parse($labtest->patient->birth_date)->diff(\Carbon\Carbon::now())->y . ' y.o./' . self::renderSex($labtest->patient->gender) . '</td>
                </tr>
                <tr >
                    <td colspan="2"><strong>DATE OF BIRTH:</strong> ' . Carbon::parse($labtest->patient->birth_date)->toFormattedDateString() . '</td>
                </tr>
                <tr >
                    <td colspan="2"><strong>REQUESTING PHYSICIAN: </strong>' . $physician . '</td>
                </tr>
            </table>
            <h1 style="text-align: center;">LABORATORY TEST RESULT</h1>
            <table style="text-align:center;" >
                <tr>
                    <td><strong>TEST</strong></td>
                    <td><strong>RESULT</strong></td>
                </tr>
                <tr>
                    <td style="font-size: 16pt;">' . $labtest->test->description . '</td>
                    <td style="font-size: 16pt;"><strong>' . self::renderResult($labtest->result) . '</strong></td>
                </tr> 
            </table>
            <br>
            <br>
            <br>
            <table >
                <tr>
                    <td style="width:25%;"><strong>Test Information:</strong></td>
                    <td style="width:75%;">
                        Novel Coronavirus (2019-nCoV) Antigen Detection Kit <br>
                        Qualitative determination of 2019-nCoV Antigen <br>
                        (Lateral Flow Method)
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <table >
                <tr>
                    <td style="width:25%;" ><br /><br /><strong>Brand:</strong></td>
                    <td style="width:55%;"><br /><br />
                    ' . $labtest->brand->name . '<br>
                    ' . $labtest->brand->manufacturer . '<br>
                    
                    </td>
                    <td style="width:18%;"><img src="@' . $imgdata . '" width="100" /></td>
                </tr>
            </table>
            <br>
            <table >
                <tr>
                    <td style="width:25%;"><strong>SPECIMEN:</strong></td>
                    <td style="width:75%;">' . $labtest->specimen->description . '  </td>
                </tr>
            </table>
            <br>
            <br>
            <table >
                <tr>
                    <td><strong>Note:</strong></td>
                    
                </tr>
                <tr>
                    <td style="text-align:justify; font-size:10pt;">Real Time Polymerase Chain Reaction (RT-PCR) is the gold standard for the diagnosis of COVID-19.
                    For symptomatic cases, this test is best utilized within seven (7) days of the onset of symptoms.
                    Confirmatory testing with RT-PCR is ideally performed at the same time or within forty-eight hours from
                    the time of collection. For asymptomatic cases, there is a limited data to use this test for screening or
                    to determine whether a previously confirmed case is still infectious.
                    </td>
                    
                </tr>
            </table>
            <br>
            <br>
            <table style="font-size:10pt;">
                <tr>
                    <td>Reference: CDC Interim Guidelines on using Antigen Test, September 4, 2020</td>
                    
                </tr>
            </table>
            <br>
            <br>
            <table style="font-size:10pt;">
                <tr>
                    <td>*RITM Validation:</td>
                    <td><strong>Sensitivity:</strong> CT < 30 (92.2%)</td>
                    <td><strong>Specificity:</strong> 100%:</td>
                </tr>
            </table>
        ';

        $sig_HTML = '
        <table>
            <tr>
                <td>
                    <p><strong>' . $labtest->medtech->first_name . ' ' . $labtest->medtech->middle_name . ' ' . $labtest->medtech->last_name . '</strong></p>
                    <p>License No. ' . $labtest->medtech->prc_no . '</p>
                    <p>' . $labtest->medtech->position . '</p>
                </td>
                <td>
                    <p><strong>PEDRITO Y. TAGAYUNA, MD, FPSP</strong></p>
                    <p>PRC License no. 0089935</p>
                    <p>Clinical Pathologist</p>
                </td>
            </tr>
        </table>
        ';


        PDF::SetTitle($labtest->id);
        PDF::AddPage();
        PDF::writeHTML($html, true, false, true, false, '');
        //  writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)
        PDF::writeHTMLCell(0, 0, 27, 260, $sig_HTML, 0, 1, 0, true, 'L', true);
        PDF::Output($labtest->id . '.pdf', $request->pub ? $request->pub : "I");
    }

    public function approve($id)
    {
        $labtest = LabTestResult::find($id);
        $labtest->update(['approved_by' => \auth()->id()]);
        return redirect(\route('labTests.show', $id));
    }
    public function to_validate($id)
    {
        $labtest = LabTestResult::find($id);
        $labtest->update(['for_validation' => 1, 'approved_by' => $this->auto_approve ? 1 : null]);
        return redirect(\route('labTests.show', $id));
    }

    public function mail_patient($key)
    {

        $labtest = LabTestResult::where('key', $key)->first();

        Mail::to($labtest->patient->email)->send(new LabTestResultMail($labtest));
        return redirect(\route('labTests.index'))->with('status', 'Email sent!');
        // for preview in browser
        // return new LabTestResultMail($labtest);
    }

    public function verify($key)
    {
        $facility_details = Setting::where('item', 'facility_details')->orderBy('id', 'desc')->pluck('value')->first();
        $labtest = LabTestResult::where('key', $key)->with('patient')->first();
        $labtest['facility_details'] = json_decode($facility_details, true);
        $labtest['result'] = self::renderResult($labtest->result);
        // $middleInitial = substr($labtest->patient->middle_name, 0, 1);
        $middleInitial = '';
        if ($labtest->patient->middle_name) {

            $mNameArr = explode(' ', $labtest->patient->middle_name);
            foreach ($mNameArr as $initial) {
                $middleInitial = $middleInitial . substr($initial, 0, 1) . '.';
            }
        }
        $labtest->patient['age'] = self::calculate_age($labtest->patient->birth_date);
        $labtest->patient['name'] = $labtest->patient->first_name . " " . strtoupper($middleInitial) . " " . $labtest->patient->last_name;
        return view('labtests.verify', compact('labtest'));
        // $labtest->update(['for_validation' => 1, 'approved_by' => $this->auto_approve ? 1 : null]);
        // return redirect(\route('labTests.show', $id));
    }

    private function renderSex($sex)
    {
        if ($sex === 'M') {
            return 'male';
        } else {
            return 'female';
        }
    }

    private function renderResult($result)
    {
        if ($result === 'NEG') {
            return 'NEGATIVE';
        } else {
            return 'POSITIVE';
        }
    }

    private function calculate_age($date)
    {

        $age = Carbon::parse($date)->diff(Carbon::now())->y;

        return $age;
    }
}
