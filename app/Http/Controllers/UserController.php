<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        return view('users.form', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $path = $request->file('esign')->store('esign');
        $request->merge(['name' => $request->first_name . " " . $request->last_name, 'signature' => $path, 'password' => bcrypt($request->password)]);
        $user = User::create($request->except('_token'));
        if ($request->role === 'P') {
            $user->assignRole('pathologist');
            $user->syncPermissions(['edit lab_result details', 'approve']);
        } else {

            $user->assignRole('medTech');
            $user->syncPermissions(['edit lab_result details', 'encode']);
        }
        return redirect(\route('users.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.form', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if ($request->has('esign')) {

            $path = $request->file('esign')->store('esign');
            $request->merge(['signature' => $path]);
        }
        $request->merge(['password' => bcrypt($request->password)]);
        $user->update($request->except('_token', 'role'));
        $user->removeRole($user->getRoleNames()[0]);
        if ($request->role === 'P') {
            $user->assignRole('pathologist');
            $user->syncPermissions(['edit lab_result details', 'approve']);
        } else {

            $user->assignRole('medTech');
            $user->syncPermissions(['edit lab_result details', 'encode']);
        }
        return redirect(\route('users.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        User::destroy($user->id);
        return redirect(\route('users.index'));
    }
}
