<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;
use App\Models\Facility;

class SettingController extends Controller
{
    public function index()
    {
        $facility_details = Setting::where('item', 'facility_details')->orderBy('created_at', 'desc')->pluck('value')->first();
        $parsed = json_decode($facility_details, TRUE);
        $pdf_logo = Setting::where('item', 'pdf_logo')->orderBy('created_at', 'desc')->first();
        $auto_approve = Setting::where('item', 'auto_approve')->orderBy('created_at', 'desc')->first();
        return view('settings.index', compact('pdf_logo', 'auto_approve', 'parsed'));
    }

    public function updateFacility(Request $request)
    {
        // Storage::disk('local')->put('file.txt', 'Contents');
        $facility_details = Setting::where('item', 'facility_details')->pluck('value')->first();
        $decoded = json_decode($facility_details, TRUE);
        if ($request->file('facility_logo')) {

            $path = $request->file('facility_logo')->store('facility_logo');
            $request->merge(['logo' => $path]);
        } else {

            $request->merge(['logo' => $decoded['logo']]);
        }
        Setting::where('item', 'facility_details')->update(['value' => $request->except('_token', 'facility_logo'), 'added_by' => \auth()->id()]);
        return redirect(\route('settings.index'));
    }
    public function updateLogo(Request $request)
    {
        // Storage::disk('local')->put('file.txt', 'Contents');

        $path = $request->file('logo')->store('pdf_logos');
        $request->merge(['value' => $path]);
        Setting::create($request->except('_token'));
        return redirect(\route('settings.index'));
    }
    public function updateAutoApprove(Request $request)
    {
        $request->merge(['value' => isset($request->value) ? 1 : 0]);
        Setting::updateOrCreate(['id' => $request->id], $request->all());
        return redirect(\route('settings.index'));
    }
}
