<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Physician;

class PhysicianController extends Controller
{
    public function index()
    {
        $physicians = Physician::all();
        return view('physicians.index', compact('physicians'));
    }

    public function create()
    {
        $physician = new Physician();
        return view('physicians.form', compact('physician'));
    }

    public function store(Request $request)
    {
        Physician::create($request->except('_token'));
        return redirect(\route('physicians.index'));
    }

    public function edit(Physician $physician)
    {
        return view('physicians.form', compact('physician'));
    }

    public function update(Request $request, Physician $physician)
    {
        $physician->update($request->except('_token'));
        return redirect(\route('physicians.index'));
    }

    public function destroy(Physician $physician)
    {
        Physician::destroy($physician->id);
        return redirect(\route('physicians.index'));
    }
}
