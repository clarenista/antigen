<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;


use App\Models\LabTestResult;
use App\Models\LabTestType;
use App\Exports\LabResultsExport;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    public function index()
    {
        $labtests = LabTestResult::with('patient.vaccinations', 'patient.province_details', 'patient.citymun_details', 'patient.barangay_details', 'exposure_history', 'symptoms', 'xray', 'healthStatus.health_status_type', 'healthStatus.disposition_type')->get();
        // foreach ($labtests as $test) {
        //     $test->labTestResults = json_decode($test->labTestResults, true);
        //     $res = $test->labTestResults;
        //     foreach ($res as $key => $value) {
        //         $res[$key]['type_description'] = LabTestType::select('description')->find($value['test_type_id']);
        //     }
        // }
        // return $labtests;
        $now = \Carbon\Carbon::now()->format('Ymd_H:i:s');
        return Excel::download(new LabResultsExport, $now . '_Antigen-Linelist.xlsx');
    }
}
