<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Patient;
use App\Models\Vaccination;

class PatientController extends Controller
{
    public function index()
    {
        $patients = Patient::with('vaccinations')->get();
        return view('patients.index', compact('patients'));
    }

    public function create(Patient $patient)
    {
        return view('patients.form', compact('patient'));
    }

    public function store(Request $request)
    {
        $request->merge(['last_name' => strtoupper($request->last_name)]);
        Patient::create($request->except('_token'));
        return redirect(\route('patients.index'));
    }

    public function edit(Patient $patient)
    {
        return view('patients.form', compact('patient'));
    }

    public function update(Request $request, Patient $patient)
    {
        $request->merge(['last_name' => strtoupper($request->last_name)]);
        $patient->update($request->except('_token'));
        return redirect(\route('patients.index'));
    }

    public function destroy(Patient $patient)
    {
        Patient::destroy($patient->id);
        return redirect(\route('patients.index'));
    }


    public function index_vax($id)
    {
        $patient = Patient::find($id);
        return view('patients.vaccination.index', compact('patient'));
    }

    public function create_vax($id)
    {
        $patient = Patient::find($id);
        $vaccine = new Vaccination();
        return view('patients.vaccination.form', compact('patient', 'vaccine'));
    }
    public function store_vax(Request $request, $id)
    {

        $patient = Patient::find($id);
        $is_adverse_event = $request->has('is_adverse_event') ? 1 : 0;
        $request['is_adverse_event'] = $is_adverse_event;
        $patient->vaccinations()->create($request->except('_token'));
        return redirect(\route('patients.index_vax', $patient->id));
    }

    public function edit_vax($id, $vax_id)
    {
        $patient = Patient::find($id);
        $vaccine = $patient->vaccinations()->whereId($vax_id)->first();
        return view('patients.vaccination.form', compact('patient', 'vaccine'));
    }

    public function update_vax(Request $request, $id, $vax_id)
    {

        $patient = Patient::find($id);
        $is_adverse_event = $request->has('is_adverse_event') ? 1 : 0;
        $request['is_adverse_event'] = $is_adverse_event;
        $patient->vaccinations()->whereId($vax_id)->update($request->except('_token', '_method'));
        return redirect(\route('patients.index_vax', $patient->id));
    }
}
