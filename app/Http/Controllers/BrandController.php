<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;

class BrandController extends Controller
{
    public function index(Brand $brand)
    {
        $brands = Brand::all();
        return view('brands.index', compact('brands', 'brand'));
    }

    public function store(Request $request)
    {
        Brand::create($request->except('_token'));
        return redirect(\route('brands.index'));
    }
    public function edit(Brand $brand)
    {
        $brands = Brand::all();
        return view('brands.index', compact('brands', 'brand'));
    }


    public function update(Request $request, Brand $brand)
    {
        $brand->update($request->except('_token'));
        return redirect(\route('brands.index'));
    }
    public function destroy(Brand $brand)
    {
        Brand::destroy($brand->id);
        return redirect(\route('brands.index'));
    }
}
