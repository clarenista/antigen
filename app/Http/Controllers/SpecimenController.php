<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Specimen;

class SpecimenController extends Controller
{
    public function index(Specimen $specimen)
    {
        $specimens = Specimen::all();
        return view('specimens.index', compact('specimens', 'specimen'));
    }

    public function store(Request $request)
    {
        Specimen::create($request->except('_token'));
        return redirect(\route('specimens.index'));
    }
    public function edit(Specimen $specimen)
    {
        $specimens = Specimen::all();
        return view('specimens.index', compact('specimens', 'specimen'));
    }


    public function update(Request $request, Specimen $specimen)
    {
        $specimen->update($request->except('_token'));
        return redirect(\route('specimens.index'));
    }
    public function destroy(Specimen $specimen)
    {
        Specimen::destroy($specimen->id);
        return redirect(\route('specimens.index'));
    }
}
