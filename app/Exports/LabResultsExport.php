<?php

namespace App\Exports;

use App\Models\LabTestResult;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class LabResultsExport implements FromCollection, WithMapping, WithHeadings, WithEvents,  WithStyles
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $id;

    function __construct($id)
    {
        $this->id = $id;
    }

    public function collection()
    {
        $labtest = LabTestResult::with('patient.vaccinations', 'patient.province_details', 'patient.citymun_details', 'patient.barangay_details', 'exposure_history', 'symptoms', 'xray', 'healthStatus.health_status_type', 'healthStatus.disposition_type')->whereIn('id', $this->id)->get();
        // dd($labtest);
        return $labtest;
        // return LabTestResult::with('patient.vaccinations', 'patient.province_details', 'patient.citymun_details', 'patient.barangay_details', 'exposure_history', 'symptoms', 'xray', 'healthStatus.health_status_type', 'healthStatus.disposition_type')->get();
    }
    public function map($labtest): array
    {
        $labtestsRes = json_decode($labtest->labTestResults, true);

        return [
            '',
            $labtest->patient->last_name,
            $labtest->patient->first_name,
            $labtest->patient->middle_name,
            $labtest->patient->suffix,
            $labtest->patient->birth_date,
            $labtest->patient->gender === 'M' ? 'Male' : 'Female',
            $labtest->patient->nationality,
            'Include occupation',
            $labtest->patient->address,
            $labtest->patient->province_details->provDesc,
            $labtest->patient->citymun_details->citymunDesc,
            $labtest->patient->barangay_details->brgyDesc,
            $labtest->patient->contact_no,
            'Include province DRU',
            'Include name DRU',
            'Include DRU date report',

            // Exposure history
            self::renderYesNo($labtest->exposure_history->is_health_worker),
            self::renderYesNo($labtest->exposure_history->is_returning_overseas),
            self::renderYesNo($labtest->exposure_history->history_exposure),
            $labtest->exposure_history->date_of_last_exposure,
            self::renderYesNo($labtest->exposure_history->been_in_a_place),
            $labtest->exposure_history->place_of_exposure,
            $labtest->exposure_history->travel_date_from,
            $labtest->exposure_history->travel_date_to,

            // Symptoms
            self::renderYesNo($labtest->symptoms->fever),
            self::renderYesNo($labtest->symptoms->cough),
            self::renderYesNo($labtest->symptoms->fatigue),
            self::renderYesNo($labtest->symptoms->headache),
            self::renderYesNo($labtest->symptoms->myalgia),
            self::renderYesNo($labtest->symptoms->sore_throat),
            self::renderYesNo($labtest->symptoms->coryza),
            self::renderYesNo($labtest->symptoms->nausea),
            self::renderYesNo($labtest->symptoms->diarrhea),
            self::renderYesNo($labtest->symptoms->altered_mental_status),
            self::renderYesNo($labtest->symptoms->difficulty_of_breathing),
            self::renderYesNo($labtest->symptoms->anosmia),
            self::renderYesNo($labtest->symptoms->ageusia),
            $labtest->symptoms->other_symptoms,
            $labtest->symptoms->onset_of_illness,

            // Xray
            self::renderYesNo($labtest->xray->is_done),
            $labtest->xray->xray_date,
            $labtest->xray->result,
            $labtest->xray->others_xray_result,
            $labtest->xray->ct_result,
            $labtest->xray->others_ct_result,
            $labtest->xray->lung_result,
            $labtest->xray->others_lung_result,

            // LabTests 1
            count($labtestsRes) > 0 ? $labtestsRes[0]['specimen_collection_date'] : '',
            count($labtestsRes) > 0 ? $labtestsRes[0]['specimen_received_date'] : '',
            count($labtestsRes) > 0 && $labtestsRes[0]['test_type_id'] ? \App\Models\LabTestType::find($labtestsRes[0]['test_type_id'])->description : '',
            count($labtestsRes) > 0 ? $labtestsRes[0]['kit_brand'] : '',
            count($labtestsRes) > 0 ? $labtestsRes[0]['reason'] : '',
            '',
            count($labtestsRes) > 0 ? $labtestsRes[0]['result_date'] : '',
            count($labtestsRes) > 0 ? $labtestsRes[0]['result'] : '',
            count($labtestsRes) > 0 ? $labtestsRes[0]['testing_lab'] : '',

            // LabTest 2
            count($labtestsRes) > 1 ? $labtestsRes[1]['specimen_collection_date'] : '',
            count($labtestsRes) > 1 ? $labtestsRes[1]['specimen_received_date'] : '',
            count($labtestsRes) > 1 && $labtestsRes[1]['test_type_id'] ? \App\Models\LabTestType::find($labtestsRes[1]['test_type_id'])->description : '',
            count($labtestsRes) > 1 ? $labtestsRes[1]['kit_brand'] : '',
            count($labtestsRes) > 1 ? $labtestsRes[1]['reason'] : '',
            '',
            count($labtestsRes) > 1 ? $labtestsRes[1]['result_date'] : '',
            count($labtestsRes) > 1 ? $labtestsRes[1]['result'] : '',
            count($labtestsRes) > 1 ? $labtestsRes[1]['testing_lab'] : '',

            // Health Status
            $labtest->healthStatus->health_status_type ? $labtest->healthStatus->health_status_type->description : '',
            $labtest->healthStatus->disposition_type ? $labtest->healthStatus->disposition_type->description : '',
            $labtest->healthStatus->admitted_date,
            $labtest->healthStatus->facility_province_first_admitted,
            $labtest->healthStatus->facility_name,
            $labtest->healthStatus->outcome,
            $labtest->healthStatus->recovery_date,
            $labtest->healthStatus->die_date,
            $labtest->healthStatus->cause_of_death,
            $labtest->healthStatus->classification,

            // vaccinations
            count($labtest->patient->vaccinations) > 0 ? $labtest->patient->vaccinations[0]->vaccination_date : '',
            count($labtest->patient->vaccinations) > 0 ? $labtest->patient->vaccinations[0]->vaccine_name : '',
            count($labtest->patient->vaccinations) > 0 ? $labtest->patient->vaccinations[0]->vaccination_facility : '',
            count($labtest->patient->vaccinations) > 0 ? $labtest->patient->vaccinations[0]->facility_region : '',
            count($labtest->patient->vaccinations) > 0 ? self::renderYesNo($labtest->patient->vaccinations[0]->is_adverse_event) : '',

            count($labtest->patient->vaccinations) > 1 ? $labtest->patient->vaccinations[1]->vaccination_date : '',
            count($labtest->patient->vaccinations) > 1 ? $labtest->patient->vaccinations[1]->vaccine_name : '',
            count($labtest->patient->vaccinations) > 1 ? $labtest->patient->vaccinations[1]->vaccination_facility : '',
            count($labtest->patient->vaccinations) > 1 ? $labtest->patient->vaccinations[1]->facility_region : '',
            count($labtest->patient->vaccinations) > 1 ? self::renderYesNo($labtest->patient->vaccinations[1]->is_adverse_event) : '',

        ];
    }

    private function renderYesNo($value)
    {
        if ($value === 1) {
            return 'Yes';
        } else {
            return 'No';
        }
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                // $event->writer->setCreator('Antigen');
            },
            AfterSheet::class    => function (AfterSheet $event) {

                $event->sheet->getDelegate()->mergeCells('A1:Q1');
                $event->sheet->getDelegate()->mergeCells('R1:Y1');
                $event->sheet->getDelegate()->mergeCells('Z1:AN1');
                $event->sheet->getDelegate()->mergeCells('AO1:AV1');
                $event->sheet->getDelegate()->mergeCells('AW1:BE1');
                $event->sheet->getDelegate()->mergeCells('BF1:BN1');
                $event->sheet->getDelegate()->mergeCells('BO1:BX1');
                $event->sheet->getDelegate()->mergeCells('BY1:CC1');
                $event->sheet->getDelegate()->mergeCells('CD1:CH1');
            },
        ];
    }
    public function headings(): array
    {
        return [
            ['Registration', '', '', 'X-ray Results', 'Laboratory Results (1st)', 'Laboratory Results (2nd)', 'Outcome and Health Status Details', 'Vaccination Information (1st)', 'Vaccination Information (2nd)'],
            [
                'RESU ID',
                'Last Name',
                'First Name',
                'Middle Name',
                'Suffix',
                'Date of Birth',
                'Sex at Birth',
                'Nationality',
                'Occupation',
                'Current House No. / Lot / Bldg. / Street',
                'Current Province',
                'Current Municipality or City',
                'Current Barangay',
                'Current Home/Cell Phone Number',
                'Province of Disease Reporting Unit',
                'Name of Disease Reporting Unit',
                'Date Reported',
                'Health Worker',
                'Returning Overseas Filipino',
                'History of exposure to known probable and/or confirmed COVID-19 case 14 days before the onset of signs and symptoms?',
                'Date of Last Exposure to Known Probable and/or Confirmed Case',
                'Have you been in a place with a Known COVID-19 community transmission 14 days before the onset of signs and symptoms?',
                'Place of Exposure',
                'Travel dates (from)',
                'Travel dates (to)',
                'Fever',
                'Cough',
                'General Weakness / Fatigue',
                'Headache',
                'Myalgia',
                'Sore Throat',
                'Coryza',
                'Anorexia / Nausea / Vomiting',
                'Diarrhea',
                'Altered Mental Status',
                'Difficulty of Breathing / Shortness of Breath',
                'Anosmia (Loss of Smell)',
                'Ageusia (Loss of Taste)',
                'Other Signs and Symptoms (excluding those specified)',
                'Date of Onset of Illness',
                'Chest X-ray Done?',
                'Date when Chest X-ray was Done',
                'Chest X-Ray Results',
                'Others, please specify',
                'Chest CT Results',
                'Others, please specify',
                'Lung Ultrasound Results',
                'Others, please specify',
                'Date Specimen Collected',
                'Date Specimen Received by Laboratory',
                'Type of Test',
                'Brand of Kit',
                'Reason for Antigen Testing',
                'Reason for Antigen Testing (Others)',
                'Date of Release of Result',
                'Test Result',
                'Lab where Testing was Done / Health Facility',
                'Date Specimen Collected',
                'Date Specimen Received by Laboratory',
                'Type of Test',
                'Brand of Kit',
                'Reason for Antigen Testing',
                'Reason for Antigen Testing (Others)',
                'Date of Release of Result',
                'Test Result',
                'Lab where Testing was Done / Health Facility',
                'Health Status',
                'Disposition of the Case',
                'Date Admitted/Isolated/Discharge',
                'Province of Facility where patient was first admitted',
                'Name of Facility where patient was first admitted',
                'Outcome',
                'Date Recovered',
                'Date Died',
                'Cause of Death',
                'Classification',
                'Date of vaccination',
                'Name of Vaccine',
                'Vaccination center/facility',
                'Region of health facility',
                'Adverse event/s Yes/No',
                'Date of vaccination',
                'Name of Vaccine',
                'Vaccination center/facility',
                'Region of health facility',
                'Adverse event/s Yes/No'


            ]
        ];
    }



    public function styles(Worksheet $sheet)
    {

        $styleArray = [
            'font' => [
                'bold' => true,
                'size' => 9
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
        ];
        $sheet->getStyle('A1:CH1')->applyFromArray($styleArray);
        $sheet->getStyle('A2:CH2')->applyFromArray($styleArray);
        $sheet->getCell('R1')->setValue('Exposure History');
        $sheet->getCell('Z1')->setValue('Signs and Symptoms');
        $sheet->getCell('AO1')->setValue('X-ray Results');
        $sheet->getCell('AW1')->setValue('Laboratory Results (1st)');
        $sheet->getCell('BF1')->setValue('Laboratory Results (2nd)');
        $sheet->getCell('BO1')->setValue('Outcome and Health Status Details');
        $sheet->getCell('BY1')->setValue('Vaccination Information (1st)');
        $sheet->getCell('CD1')->setValue('Vaccination Information (2nd)');
        $sheet->getDefaultColumnDimension()->setWidth(13.71);
        $sheet->getStyle('A2:CH2')->getAlignment()->setWrapText(true);
        $sheet->getRowDimension('2')->setRowHeight(98.25);
        $sheet->getStyle('A2:CH2')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    }
}
