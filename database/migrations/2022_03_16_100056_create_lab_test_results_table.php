<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLabTestResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lab_test_results', function (Blueprint $table) {
            $table->id();
            $table->date('date')->nullable();
            $table->time('time')->nullable();
            $table->integer('patient_id')->nullable();
            // $table->text('address')->nullable();
            $table->string('email_address')->nullable();
            $table->integer('requesting_physician_id')->nullable();
            $table->integer('test_id')->nullable();
            $table->string('result')->nullable();
            $table->text('test_information')->nullable();
            $table->integer('brand_id')->nullable();
            $table->integer('specimen_id')->nullable();
            $table->text('note')->nullable();
            $table->integer('medtech_id')->nullable();
            $table->text('comment')->nullable();
            $table->integer('approved_by')->nullable();
            $table->boolean('isEmailed')->nullable()->default(0);
            $table->text('labTestResults')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lab_test_results');
    }
}
