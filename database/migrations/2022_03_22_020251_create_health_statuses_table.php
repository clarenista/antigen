<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHealthStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('health_statuses', function (Blueprint $table) {
            $table->id();
            $table->integer('patient_id');
            $table->integer('lab_test_result_id');
            $table->integer('health_status_type_id')->nullable();
            $table->integer('disposition_type_id')->nullable();
            $table->date('admitted_date')->nullable();
            $table->string('facility_province_first_admitted')->nullable();
            $table->string('facility_name')->nullable();
            $table->string('outcome')->nullable();
            $table->date('recovery_date')->nullable();
            $table->date('die_date')->nullable();
            $table->text('cause_of_death')->nullable();
            $table->string('classification')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('health_statuses');
    }
}
