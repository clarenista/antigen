<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDateTimeInLabTestResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lab_test_results', function (Blueprint $table) {
            $table->boolean('for_validation')->default(0);
            $table->text('verified_by')->nullable();
            $table->timestamp('verified_at')->nullable();
            $table->text('key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lab_test_results', function (Blueprint $table) {
            //
        });
    }
}
