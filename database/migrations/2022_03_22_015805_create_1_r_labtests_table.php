<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Create1RLabtestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('1_r_labtests', function (Blueprint $table) {
            $table->id();
            $table->integer('patient_id');
            $table->integer('lab_test_result_id');
            $table->date('specimen_collection_date')->nullable();
            $table->date('specimen_received_date')->nullable();
            $table->string('test_type')->nullable();
            $table->string('kit_brand')->nullable();
            $table->text('reason')->nullable();
            $table->date('result_date')->nullable();
            $table->string('result')->nullable();
            $table->text('testing_lab')->nullable();
            $table->string('times')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('1_r_labtests');
    }
}
