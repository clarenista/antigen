<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientSymptomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_symptoms', function (Blueprint $table) {
            $table->id();
            $table->integer('patient_id');
            $table->integer('lab_test_result_id');
            $table->boolean('fever')->default(0);
            $table->boolean('cough')->default(0);
            $table->boolean('fatigue')->default(0);
            $table->boolean('headache')->default(0);
            $table->boolean('myalgia')->default(0);
            $table->boolean('sore_throat')->default(0);
            $table->boolean('coryza')->default(0);
            $table->boolean('nausea')->default(0);
            $table->boolean('diarrhea')->default(0);
            $table->boolean('altered_mental_status')->default(0);
            $table->boolean('difficulty_of_breathing')->default(0);
            $table->boolean('anosmia')->default(0);
            $table->boolean('ageusia')->default(0);
            $table->text('other_symptoms')->nullable();
            $table->date('onset_of_illness')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_symptoms');
    }
}
