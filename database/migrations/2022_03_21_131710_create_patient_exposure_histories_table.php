<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientExposureHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_exposure_histories', function (Blueprint $table) {
            $table->id();
            $table->integer('patient_id');
            $table->integer('lab_test_result_id');
            $table->boolean('is_health_worker')->default(0);
            $table->boolean('is_returning_overseas')->default(0);
            $table->boolean('history_exposure')->default(0);
            $table->date('date_of_last_exposure')->nullable();
            $table->boolean('been_in_a_place')->default(0);
            $table->text('place_of_exposure')->nullable();
            $table->date('travel_date_from')->nullable();
            $table->date('travel_date_to')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_exposure_histoties');
    }
}
