<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateXraysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xrays', function (Blueprint $table) {
            $table->id();
            $table->integer('patient_id');
            $table->integer('lab_test_result_id');
            $table->boolean('is_done')->default(0);
            $table->date('xray_date')->nullable();
            $table->string('result')->nullable();
            $table->string('others_xray_result')->nullable();
            $table->string('ct_result')->nullable();
            $table->string('others_ct_result')->nullable();
            $table->string('lung_result')->nullable();
            $table->string('others_lung_result')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xrays');
    }
}
