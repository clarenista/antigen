<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CityMun;

class CityMunsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CityMun::truncate();
        $csvFile = fopen(base_path("database/seeders/csv/refcitymun.csv"), "r");

        $firstline = true;
        // Get current data from items table
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                $citymun = CityMun::create([
                    "psgcCode" => $data[1],
                    "citymunDesc" => $data[2],
                    "regDesc" => $data[3],
                    "provCode" => $data[4],
                    "citymunCode" => $data[5],
                ]);
                echo 'done ' . $citymun->citymunDesc . PHP_EOL;
            }
            $firstline = false;
        }

        fclose($csvFile);
    }
}
