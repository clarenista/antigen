<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Setting::create(['item' => 'pdf_logo', 'value' => 'logos']);
        \App\Models\Setting::create(['item' => 'auto_approve', 'value' => 0]);
        \App\Models\Setting::create(['item' => 'facility_details', 'value' => '{"name": "Facility Name","contact":"1234","type": "Hospital","address": "1234 Address","logo": "LOGO"}']);
    }
}
