<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Province;

class ProvincesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Province::truncate();
        $csvFile = fopen(base_path("database/seeders/csv/refprovince.csv"), "r");

        $firstline = true;
        // Get current data from items table
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                $province = Province::create([
                    "psgcCode" => $data[1],
                    "provDesc" => $data[2],
                    "regCode" => $data[3],
                    "provCode" => $data[4],
                ]);
                echo 'done ' . $province->id . PHP_EOL;
            }
            $firstline = false;
        }

        fclose($csvFile);
    }
}
