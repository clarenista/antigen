<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;


class PhysiciansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Physician::create([
            'first_name' => 'ORBEN',
            'middle_name' => 'F.V',
            'last_name' => 'BERNARDO',
            'suffix' => 'MD',
            'prc_no' => '123',

        ]);
    }
}
