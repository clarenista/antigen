<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PatientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Patient::create([
            'first_name' => 'Adrian',
            'middle_name' => 'Lacap',
            'last_name' => 'VALENZUELA',
            'gender' => 'M',
            'email' => 'adrian@test.com.ph',
            'birth_date' => '1989-06-06',
            'contact_no' => '123',
            'address' => '1233 blg',
            'province' => 1,
            'citymun' => 1,
            'barangay' => 1

        ]);
    }
}
