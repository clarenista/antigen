<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Region;

class RegionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Region::truncate();
        $csvFile = fopen(base_path("database/seeders/csv/refregion.csv"), "r");

        $firstline = true;
        // Get current data from items table
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                $region = Region::create([
                    "psgcCode" => $data[1],
                    "regDesc" => $data[2],
                    "regCode" => $data[3],
                ]);
                echo 'done ' . $region->regDesc . PHP_EOL;
            }
            $firstline = false;
        }

        fclose($csvFile);
    }
}
