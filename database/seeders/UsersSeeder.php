<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'encode']);
        Permission::create(['name' => 'approve']);
        Permission::create(['name' => 'edit lab_result details']);
        Permission::create(['name' => 'edit lab_result date']);
        Permission::create(['name' => 'edit lab_result time']);
        Permission::create(['name' => 'edit lab_result patient']);
        Permission::create(['name' => 'edit lab_result addess']);
        Permission::create(['name' => 'edit lab_result requesting_physician']);
        Permission::create(['name' => 'edit lab_result test']);
        Permission::create(['name' => 'edit lab_result result']);
        Permission::create(['name' => 'edit lab_result test_information']);
        Permission::create(['name' => 'edit lab_result brand']);
        Permission::create(['name' => 'edit lab_result specimen']);
        Permission::create(['name' => 'edit lab_result note']);
        Permission::create(['name' => 'edit lab_result medtech']);
        Permission::create(['name' => 'edit lab_result comment']);


        Permission::create(['name' => 'add lab_result']);
        Permission::create(['name' => 'submit lab_result validation']);
        Permission::create(['name' => 'manage patients']);
        Permission::create(['name' => 'manage physicians']);
        Permission::create(['name' => 'manage brands']);
        Permission::create(['name' => 'manage settings']);


        // create roles and assign existing permissions
        $pathoRole = Role::create(['name' => 'pathologist']);
        $pathoRole->givePermissionTo('edit lab_result details');
        $pathoRole->givePermissionTo('approve');
        $pathoRole->givePermissionTo('manage patients');
        $pathoRole->givePermissionTo('manage physicians');
        $pathoRole->givePermissionTo('manage brands');
        $pathoRole->givePermissionTo('manage settings');


        $medTechRole = Role::create(['name' => 'medTech']);
        $medTechRole->givePermissionTo('encode');
        $medTechRole->givePermissionTo('add lab_result');
        $medTechRole->givePermissionTo('submit lab_result validation');
        $medTechRole->givePermissionTo('manage patients');
        $medTechRole->givePermissionTo('manage physicians');

        $patho = \App\Models\User::create([
            'name' => 'Pedrito Y. Tagayuna',
            'first_name' => 'Pedrito',
            'middle_name' => 'Y.',
            'last_name' => 'Tagayuna',
            'prc_no' => '0089935',
            'position' => 'Clinical Pathologist',
            'signature' => 'Tagayuna',
            'email' => 'bobongmd@yahoo.com',
            'password' => bcrypt('123')
        ]);

        $patho->assignRole($pathoRole);

        $medtech = \App\Models\User::create([
            'name' => 'Rachelle P. Red',
            'first_name' => 'RACHELLE',
            'middle_name' => 'P.',
            'last_name' => 'RED, RMT',
            'prc_no' => '0078632',
            'position' => 'Medical Technologist',
            'signature' => 'Tagayuna',
            'email' => 'medtech@yahoo.com',
            'password' => bcrypt('123')
        ]);
        $medtech->assignRole($medTechRole);
    }
}
