<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PhysiciansSeeder::class,
            UsersSeeder::class,
            PatientSeeder::class,
            ProvincesSeeder::class,
            CityMunsSeeder::class,
            BarangaysSeeder::class,
            RegionsSeeder::class,
            SettingsSeeder::class,
        ]);
        // Brand
        \App\Models\Brand::create([
            'name' => 'Wondfo',
            'manufacturer' => 'Guangzhou Wondfo Biotech Co.'
        ]);
        // Specimen
        \App\Models\Specimen::create([
            'description' => 'Nasopharyngeal/Oropharyngeal swab',
        ]);
        // Test
        \App\Models\Test::create([
            'description' => 'SARS-CoV-2 Antigen',
        ]);

        // Lab Result Test Type
        \App\Models\LabTestType::create([
            'description' => 'Antigen Test',
        ]);
        \App\Models\LabTestType::create([
            'description' => 'rRPAT ASSAY',
        ]);
        \App\Models\LabTestType::create([
            'description' => 'Rapid Antibody',
        ]);
        \App\Models\LabTestType::create([
            'description' => 'RT-PCR',
        ]);
        \App\Models\LabTestType::create([
            'description' => 'GeneXpert-COVID',
        ]);
        \App\Models\LabTestType::create([
            'description' => 'Rapid Test IgG',
        ]);
        \App\Models\LabTestType::create([
            'description' => 'Rapid test IgM',
        ]);
        \App\Models\LabTestType::create([
            'description' => 'ELISA',
        ]);
        \App\Models\LabTestType::create([
            'description' => 'others',
        ]);

        // health Status
        \App\Models\HealthStatusType::create([
            'description' => 'Asymptomatic',
        ]);
        \App\Models\HealthStatusType::create([
            'description' => 'Mild',
        ]);
        \App\Models\HealthStatusType::create([
            'description' => 'Moderate',
        ]);
        \App\Models\HealthStatusType::create([
            'description' => 'Severe',
        ]);
        \App\Models\HealthStatusType::create([
            'description' => 'Critical',
        ]);

        // Disposition
        \App\Models\DispositionType::create([
            'description' => 'Admitted in a Health Facility',
        ]);
        \App\Models\DispositionType::create([
            'description' => 'Quarantined in a Health Facility',
        ]);
        \App\Models\DispositionType::create([
            'description' => 'In Home Isolation',
        ]);
        \App\Models\DispositionType::create([
            'description' => 'Discharged',
        ]);
        \App\Models\DispositionType::create([
            'description' => 'Transferred to another Facility',
        ]);
        \App\Models\DispositionType::create([
            'description' => 'Lost to Follow-Up',
        ]);
    }
}
