<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Barangay;

class BarangaysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Barangay::truncate();
        $csvFile = fopen(base_path("database/seeders/csv/refbrgy.csv"), "r");

        $firstline = true;
        // Get current data from items table
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                $barangay = Barangay::create([
                    "brgyCode" => $data[1],
                    "brgyDesc" => $data[2],
                    "regCode" => $data[3],
                    "provCode" => $data[4],
                    "citymunCode" => $data[5],
                ]);
                echo 'done ' . $barangay->brgyDesc . PHP_EOL;
            }
            $firstline = false;
        }

        fclose($csvFile);
    }
}
